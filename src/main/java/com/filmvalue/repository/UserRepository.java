/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.repository;

import com.filmvalue.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Oriol
 */
public interface UserRepository extends JpaRepository<User, Long> {
     User findByUsername(String email);
}
