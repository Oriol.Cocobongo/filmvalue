/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.repository;

import com.filmvalue.model.Rate;
import java.util.List;
import net.bytebuddy.TypeCache.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oriol
 */
@Repository
public interface RateRepository extends JpaRepository<Rate, Long> {

    @Query(value = "SELECT * FROM tbl_rates WHERE id_film = :idFilm " , nativeQuery = true)
    List<Rate> getRateByFilmId(@Param("idFilm") String idFilm);

}
