/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.filmvalue.model.Film;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
 * CRUD refers Create, Read, Update, Delete
 *
 * @author Grup 4
 */

@Repository

public interface FilmRepository extends JpaRepository<Film, String> {
    
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_avg DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopTen();//(@Param("category") String category);
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_acting DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopCategory(@Param("category") String category);
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_acting DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopActing();
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_edition DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopEdition();
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_script DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopScript();
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_sound DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopSound();
    @Query(value = "SELECT * FROM tbl_film WHERE total_rates > 0 ORDER BY rate_visuals DESC LIMIT 10" , nativeQuery = true)
    List<Film> getTopVisuals();
}