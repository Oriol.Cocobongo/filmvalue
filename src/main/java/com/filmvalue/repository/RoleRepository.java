/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.repository;

import com.filmvalue.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Oriol
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
    
}
