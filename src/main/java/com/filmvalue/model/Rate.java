/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Oriol
 */
@Entity
@Table(name = "TBL_RATES")
public class Rate {//extends Film{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idRate;

   /*@Column(name = "id_Film")
    private String idFilm;*/
    
    @ManyToOne
    @JoinColumn(name = "id_film")
    private Film film;
    

    @ManyToOne
    @JoinColumn(name = "id")
    private User user;

    @Column(name = "rate_avg")
    private Float rateGlobalValue;

    @Column(name = "rate_acting")
    private Float rateActing;

    @Column(name = "rate_visuals")
    private Float rateVisuals;

    @Column(name = "rate_edition")
    private Float rateEdition;

    @Column(name = "rate_script")
    private Float rateScript;

    @Column(name = "rate_sound")
    private Float rateSound;

    @Column(name = "review", length = 2048)
    private String review;

    public Rate() {
        super();
    }

    public Rate(/*String idFilm,*/ Float rateGlobalValue, Float rateActing, Float rateVisuals, Float rateEdition, Float rateScript, Float rateSound, String review) {
        //this.idFilm = idFilm;
        this.rateGlobalValue = rateGlobalValue;
        this.rateActing = rateActing;
        this.rateVisuals = rateVisuals;
        this.rateEdition = rateEdition;
        this.rateScript = rateScript;
        this.rateSound = rateSound;
        this.review = review;
    }

    /*public String getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(String idFilm) {
        this.idFilm = idFilm;
    }*/

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Float getRateGlobalValue() {
        return rateGlobalValue;
    }

    public void setRateGlobalValue(Float rateGlobalValue) {
        this.rateGlobalValue = rateGlobalValue;
    }

    public Float getRateActing() {
        return rateActing;
    }

    public void setRateActing(Float rateActing) {
        this.rateActing = rateActing;
    }

    public Float getRateVisuals() {
        return rateVisuals;
    }

    public void setRateVisuals(Float rateVisuals) {
        this.rateVisuals = rateVisuals;
    }

    public Float getRateEdition() {
        return rateEdition;
    }

    public void setRateEdition(Float rateEdition) {
        this.rateEdition = rateEdition;
    }

    public Float getRateScript() {
        return rateScript;
    }

    public void setRateScript(Float rateScript) {
        this.rateScript = rateScript;
    }

    public Float getRateSound() {
        return rateSound;
    }

    public void setRateSound(Float rateSound) {
        this.rateSound = rateSound;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String critic) {
        this.review = critic;
    }

    @Override
    public String toString() {
        return "Rate [Id=" + idRate 
                + ", Film=" + film.getIdFilm()
                + ", IdUser= " + user
                + //idUser +
                ", Acting=" + rateActing
                + ", Edition=" + rateEdition
                + ", Script" + rateScript
                + ", Sound=" + rateSound
                + ", Visuals=" + rateVisuals
                + ", Global Value=" + rateGlobalValue
                + ", Review=" + review
                + "]";
    }

}
