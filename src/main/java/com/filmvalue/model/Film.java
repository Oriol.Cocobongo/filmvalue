/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.model;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Oriol
 */
@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "TBL_FILM")
public class Film {

    @Id
    private String idFilm;

    @Column(name = "title")
    private String title;

    @Column(name = "year")
    private String year;
    
    @Column(name = "type")
    private String type;

    @Column(name = "poster")
    private String poster;

    @Column(name = "rate_imdb")
    private Float rateImdb;
    
    @Column(name = "director")
    private String director;
    
    @Column(name = "actors")
    private String actors;
    
    @Column(name = "plot", length = 4096)
    private String plot;

    @Column(name = "rate_avg")
    private Float rateGlobalValue;

    @Column(name = "rate_acting")
    private Float rateActing;

    @Column(name = "rate_visuals")
    private Float rateVisuals;

    @Column(name = "rate_edition")
    private Float rateEdition;

    @Column(name = "rate_script")
    private Float rateScript;

    @Column(name = "rate_sound")
    private Float rateSound;

    @Column(name = "total_rates")
    private int totalRates;

    public Film() {
        super();
    }

    public Film(String idFilm, String title, String year, String poster, Float rateImdb) {
        super();
        this.idFilm = idFilm;
        this.title = title;
        this.year = year;
        this.poster = poster;
        this.rateImdb = rateImdb;
    }

    public String getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(String idFilm) {
        this.idFilm = idFilm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Float getRateImdb() {
        return rateImdb;
    }

    public void setRateImdb(Float rateImdb) {
        this.rateImdb = rateImdb;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }
    
    

    public Float getRateGlobalValue() {
        return rateGlobalValue;
    }

    public void setRateGlobalValue(Float rateGlobalValue) {
        this.rateGlobalValue = rateGlobalValue;
    }

    public Float getRateActing() {
        return rateActing;
    }

    public void setRateActing(Float rateActing) {
        this.rateActing = rateActing;
    }

    public Float getRateVisuals() {
        return rateVisuals;
    }

    public void setRateVisuals(Float rateVisuals) {
        this.rateVisuals = rateVisuals;
    }

    public Float getRateEdition() {
        return rateEdition;
    }

    public void setRateEdition(Float rateEdition) {
        this.rateEdition = rateEdition;
    }

    public Float getRateScript() {
        return rateScript;
    }

    public void setRateScript(Float rateScript) {
        this.rateScript = rateScript;
    }

    public Float getRateSound() {
        return rateSound;
    }

    public void setRateSound(Float rateSound) {
        this.rateSound = rateSound;
    }

    public int getTotalRates() {
        return totalRates;
    }

    public void setTotalRates(int totalRates) {
        this.totalRates = totalRates;
    }

    @Override
    public String toString() {
        return "Film [Id=" + idFilm + ", Year=" + year
                + ", Poster=" + poster + "]";
    }

}
