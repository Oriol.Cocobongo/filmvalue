/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Oriol
 */
public interface UserDetailsService {
    UserDetails loadUserByUsername(String email);
    UserDetails loadUserByEmail(String email);
}
