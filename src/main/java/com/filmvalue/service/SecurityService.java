 /*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

/**
 *
 * @author Oriol
 */
public interface SecurityService {

    String findLoggedInUsername();
    void autoLogin(String email, String password);

}
