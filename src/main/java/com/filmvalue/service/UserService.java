/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

import com.filmvalue.model.Rate;
import com.filmvalue.model.User;

/**
 *
 * @author Oriol
 */
public interface UserService {

    void save(User user);

    User findByUsername(String email);
    
    void addRate(User user, Rate rate);
    
    void updateUserProfile(User user);
    
    boolean userRatedThisFilm(User user, String idFilm);
    
}
