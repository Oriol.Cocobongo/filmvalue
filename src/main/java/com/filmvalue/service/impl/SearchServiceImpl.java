package com.filmvalue.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.filmvalue.model.Film;
import com.filmvalue.repository.FilmRepository;
import com.filmvalue.repository.RateRepository;
import com.filmvalue.response.ResponseSearch;
import com.filmvalue.service.FilmService;
import com.filmvalue.service.SearchService;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
/**
 *
 * @author Oriol
 */
@Service
public class SearchServiceImpl implements SearchService {

    final int GLOBAL_VALUE = 0;
    final int ACTING = 1;
    final int VISUALS = 2;
    final int EDITION = 3;
    final int SCRIPT = 4;
    final int SOUND = 5;

    @Autowired
    ResponseSearch responseSearch;

    @Autowired
    FilmRepository filmRepository;

    @Autowired
    RateRepository rateRepository;

    @Autowired
    FilmService filmService;

    /*
    * TO-DO:
    * refactor to searchFilm()
    * get real rates from film -> calcular mediana
     */
    @Override
    public List<Film> getDadesFilmsSearch(String title) {
        List<Film> filmList = null;

        try {
            filmList = responseSearch.getDadesFilmsSearch(title);

            //Obtenim rates dels films.
            for (Film film : filmList) {

                String filmId = film.getIdFilm();

                if (filmService.existsById(filmId)) {

                    film.setRateActing(filmService.getFilmById(filmId).getRateActing());
                    film.setRateVisuals(filmService.getFilmById(filmId).getRateVisuals());
                    film.setRateEdition(filmService.getFilmById(filmId).getRateEdition());
                    film.setRateScript(filmService.getFilmById(filmId).getRateScript());
                    film.setRateSound(filmService.getFilmById(filmId).getRateSound());
                    film.setRateGlobalValue(filmService.getFilmById(filmId).getRateGlobalValue());
                    film.setTotalRates(filmService.getFilmById(filmId).getTotalRates());

                } else { //Guarda el film a la bbdd (si no existeix ja)  
                    filmService.saveFilm(film);
                }
            }

        } catch (JsonProcessingException ex) {
            Logger.getLogger(SearchServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return filmList;
    }

    @Override
    public Map getAllInfoMovie(String idFilm) { //TO-DO Refactor return Film
        //Film filmInfo = null;
        Map<String, String> filmInfo = null;

        try {
            filmInfo = responseSearch.getAllInfoMovie(idFilm);

            //guardem dades film
            Film film = filmService.getFilmById(idFilm);
            film.setActors(filmInfo.get("Actors"));
            film.setDirector(filmInfo.get("Director"));
            film.setPlot(filmInfo.get("Plot"));
            if (filmInfo.get("imdbRating").equalsIgnoreCase("N/A")) {
                 film.setRateImdb(0.0f);
            }else{
                 film.setRateImdb((Float) Float.parseFloat(filmInfo.get("imdbRating")));
            }
           
            filmService.saveFilm(film);

        } catch (JsonProcessingException ex) {
            Logger.getLogger(SearchServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return filmInfo;
    }

}
