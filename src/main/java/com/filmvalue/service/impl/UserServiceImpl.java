/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service.impl;

import com.filmvalue.model.Rate;
import com.filmvalue.model.User;
import com.filmvalue.repository.RoleRepository;
import com.filmvalue.repository.UserRepository;
import com.filmvalue.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

/**
 *
 * @author Oriol
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        user.setProfilePicture("generic.jpg");
        userRepository.save(user);
    }
    
    @Override
    public void updateUserProfile(User user) {
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String email) {
        return userRepository.findByUsername(email);
    }

    @Override
    public void addRate(User user, Rate rate) {
        user.getUserRates().add(rate);
    }

    @Override
    public boolean userRatedThisFilm(User user, String idFilm) {

        boolean voted = false;

        for (Rate r : user.getUserRates()) {
            if (r.getFilm().getIdFilm() == idFilm) {
                voted = true;
            }
        }
        return voted;
    }

}
