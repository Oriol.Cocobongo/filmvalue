/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service.impl;

import com.filmvalue.model.Film;
import com.filmvalue.model.Rate;
import com.filmvalue.repository.FilmRepository;
import com.filmvalue.repository.RateRepository;
import com.filmvalue.service.FilmService;
import com.filmvalue.service.RateService;
import com.filmvalue.service.SearchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author Oriol
 */
@Service
public class FilmServiceImpl implements FilmService {

    //TO-DO posar const a obj
    final int GLOBAL_VALUE = 0;
    final int ACTING = 1;
    final int VISUALS = 2;
    final int EDITION = 3;
    final int SCRIPT = 4;
    final int SOUND = 5;

    @Autowired
    FilmRepository filmRepository;

    @Autowired
    RateRepository rateRepository;

    @Autowired
    SearchService searchService;

    @Autowired
    RateService rateService;

    @Override
    public Boolean existsById(String filmId) {
        return filmRepository.existsById(filmId);
    }

    @Override
    public Film getFilmById(String filmId) {
        //filmRepository.existsById(film.getIdFilm())
        return filmRepository.getOne(filmId);
    }

    @Override
    public void saveFilm(Film film) {
        filmRepository.save(film);
    }

    /*
    @Override
    public void updateFilmRate(String filmId) {

        float[] newRatesValue = rateService.getNewFilmValue(filmId);

        Film updatedFilm = getFilmById(filmId);

        updatedFilm.setRateGlobalValue(newRatesValue[GLOBAL_VALUE]);
        updatedFilm.setRateActing(newRatesValue[ACTING]);
        updatedFilm.setRateVisuals(newRatesValue[VISUALS]);
        updatedFilm.setRateEdition(newRatesValue[EDITION]);
        updatedFilm.setRateScript(newRatesValue[SCRIPT]);
        updatedFilm.setRateSound(newRatesValue[SOUND]);

        updatedFilm.setTotalRates(updatedFilm.getTotalRates() + 1);

        filmRepository.save(updatedFilm);

    }*/
    @Override
    public void updateFilmRate(Rate rate) {

        //Film updatedFilm = getFilmById(rate.getIdFilm());
        //Film updatedFilm = getFilmById(rate.getFilm().getIdFilm());
        System.out.println("updateFilm");
        Film updatedFilm = rate.getFilm();

        if (updatedFilm.getTotalRates() == 0) {

            updatedFilm.setRateGlobalValue(rate.getRateGlobalValue());
            updatedFilm.setRateActing(rate.getRateActing());
            updatedFilm.setRateVisuals(rate.getRateVisuals());
            updatedFilm.setRateEdition(rate.getRateEdition());
            updatedFilm.setRateScript(rate.getRateScript());
            updatedFilm.setRateSound(rate.getRateSound());

            updatedFilm.setTotalRates(updatedFilm.getTotalRates() + 1);

            filmRepository.save(updatedFilm);

        } else {

            float[] newRatesValue = rateService.getNewFilmValue(rate.getFilm().getIdFilm());

            updatedFilm.setRateGlobalValue(newRatesValue[GLOBAL_VALUE]);
            updatedFilm.setRateActing(newRatesValue[ACTING]);
            updatedFilm.setRateVisuals(newRatesValue[VISUALS]);
            updatedFilm.setRateEdition(newRatesValue[EDITION]);
            updatedFilm.setRateScript(newRatesValue[SCRIPT]);
            updatedFilm.setRateSound(newRatesValue[SOUND]);

            updatedFilm.setTotalRates(updatedFilm.getTotalRates() + 1);

            filmRepository.save(updatedFilm);
        }
    }

    @Override
    public List<Film> getTopTen() {
        return filmRepository.getTopTen();
    }

    @Override
    public List<Film> getTopCategory(String category) {

        List<Film> categoryList = null;

        switch (category) {
            case "acting":
                categoryList = filmRepository.getTopActing();
                break;
            case "edition":
                categoryList = filmRepository.getTopEdition();
                break;
            case "script":
                categoryList = filmRepository.getTopScript();
                break;
            case "sound":
                categoryList = filmRepository.getTopSound();
                break;
            case "visuals":
                categoryList = filmRepository.getTopVisuals();
                break;
            case "global":
                categoryList = filmRepository.getTopTen();
                break;
        }
        //return filmRepository.getTopCategory(category);
        return categoryList;
    }

}
