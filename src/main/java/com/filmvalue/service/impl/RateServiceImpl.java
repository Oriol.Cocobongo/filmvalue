/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service.impl;

import com.filmvalue.model.Rate;
import com.filmvalue.repository.FilmRepository;
import com.filmvalue.repository.RateRepository;
import com.filmvalue.service.FilmService;
import com.filmvalue.service.RateService;
import com.filmvalue.service.UserService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 *
 * @author Oriol
 */
@Service
public class RateServiceImpl implements RateService {

    final int GLOBAL_VALUE = 0;
    final int ACTING = 1;
    final int VISUALS = 2;
    final int EDITION = 3;
    final int SCRIPT = 4;
    final int SOUND = 5;

    @Autowired
    RateRepository rateRepository;

    @Autowired
    FilmRepository filmRepository;

    @Autowired
    FilmService filmService;

    @Autowired
    private UserService userService;

    /*@Override
    public void saveRate(Rate rate) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        System.out.println("UserId: " + userService.findByUsername(((UserDetails) principal).getUsername()).getId());
        System.out.println("Username: " + ((UserDetails) principal).getUsername());
        //rate.setIdUser(userService.findByUsername(((UserDetails)principal).getUsername()).getId());
        //rate.setIdUser(((UserDetails)principal).getUsername());
        
        //calcula global value
        //Rate updatedRate = calcGlobalValue(rate);
        //Afegir film al rate
        //rate.setFilm(filmService.getFilmById(rate));
        //Afegeix rate al user
        userService.addRate(userService.findByUsername(((UserDetails) principal).getUsername()), calcGlobalValue(rate));
    }*/
    
    @Override
    public void saveRate(Rate rate, String idFilm) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        System.out.println("UserId: " + userService.findByUsername(((UserDetails) principal).getUsername()).getId());
        System.out.println("Username: " + ((UserDetails) principal).getUsername());
        //Afegir film al rate
        rate.setFilm(filmService.getFilmById(idFilm));
        //Afegeix rate al user
        userService.addRate(userService.findByUsername(((UserDetails) principal).getUsername()), calcGlobalValue(rate));

    }

    @Override
    public List<Rate> getFilmRates(String idFilm) {
        List<Rate> rateList = rateRepository.getRateByFilmId(idFilm);
        return rateList;
    }

    @Override
    public float[] getNewFilmValue(String idFilm) {
        return calcMedian(getFilmRates(idFilm));
    }

    @Override
    public Rate getRateById(Long idRate) {
        Rate rate = rateRepository.findById(idRate).get();
        return rate;
    }

    public Rate calcGlobalValue(Rate rate) {

        float globalValue = 0.0f;

        globalValue += rate.getRateActing();
        globalValue += rate.getRateEdition();
        globalValue += rate.getRateScript();
        globalValue += rate.getRateSound();
        globalValue += rate.getRateVisuals();

        //Calculem mitja segons les notes generades.
        rate.setRateGlobalValue(Math.round(((float) (globalValue / 5)) * 100.0f) / 100.0f);

        return rate;
    }

    /*
    * Mediana: si array és senar el valor del mig, si és parell la mitja entre els dos del mig.
    * 
    * 
     */
    public float[] calcMedian(List<Rate> rateList) {

        float[] medianValues = new float[6];

        //Map<Integer, List<Float>> rateValues = new HashMap<>();
        if (rateList.size() != 0) {

            List<Float> categoryActingValues = new ArrayList<>();
            List<Float> categoryVisualsValues = new ArrayList<>();
            List<Float> categoryEditionValues = new ArrayList<>();
            List<Float> categoryScriptValues = new ArrayList<>();
            List<Float> categorySoundValues = new ArrayList<>();
            List<Float> categoryGlobalValueValues = new ArrayList<>();

            //6 arrays 1 per cada conjunt de valors
            for (Rate rate : rateList) {

                categoryActingValues.add(rate.getRateActing());
                categoryVisualsValues.add(rate.getRateVisuals());
                categoryEditionValues.add(rate.getRateEdition());
                categoryScriptValues.add(rate.getRateScript());
                categorySoundValues.add(rate.getRateSound());
                categoryGlobalValueValues.add(rate.getRateGlobalValue());
            }

            medianValues[GLOBAL_VALUE] = calcCategoryMedian(categoryGlobalValueValues);
            medianValues[ACTING] = calcCategoryMedian(categoryActingValues);
            medianValues[VISUALS] = calcCategoryMedian(categoryVisualsValues);
            medianValues[EDITION] = calcCategoryMedian(categoryEditionValues);
            medianValues[SCRIPT] = calcCategoryMedian(categoryScriptValues);
            medianValues[SOUND] = calcCategoryMedian(categorySoundValues);

        } else {
            medianValues[GLOBAL_VALUE] = 0.0f;
            medianValues[ACTING] = 0.0f;
            medianValues[VISUALS] = 0.0f;
            medianValues[EDITION] = 0.0f;
            medianValues[SCRIPT] = 0.0f;
            medianValues[SOUND] = 0.0f;
        }
        return medianValues;
    }

    public float calcCategoryMedian(List<Float> categoryValuesList) {

        Collections.sort(categoryValuesList);
        System.out.println(categoryValuesList);

        int sizeCategoryList = categoryValuesList.size();

        if (categoryValuesList.size() == 0 || categoryValuesList.size() == 1) {
            return categoryValuesList.get(0);
        }
        if (categoryValuesList.size() == 2) {
            return (categoryValuesList.get(0) + categoryValuesList.get(1)) / 2.0f;
        } else {
            switch (sizeCategoryList % 2) {
                case 0: //parell
                    float primerValor = categoryValuesList.get((sizeCategoryList / 2));
                    float segonValor = categoryValuesList.get((sizeCategoryList / 2) + 1);
                    return (primerValor + segonValor) / 2.0f;
                case 1://imparell
                    return categoryValuesList.get(((sizeCategoryList - 1) / 2) + 1);
            }

        }
        return 0.0f;
    }

    //Segona opció, millor però encara no funcional
    @Override
    public Rate calcNewFilmValueRate(Rate rate) {

        Rate updatedGlobalRate = new Rate();
        List<Rate> rateList = rateRepository.getRateByFilmId(rate.getFilm().getIdFilm());

        if (rateList.size() > 0) {

            float[] medianValues = new float[6];

            List<Float> categoryActingValues = new ArrayList<>();
            List<Float> categoryVisualsValues = new ArrayList<>();
            List<Float> categoryEditionValues = new ArrayList<>();
            List<Float> categoryScriptValues = new ArrayList<>();
            List<Float> categorySoundValues = new ArrayList<>();
            List<Float> categoryGlobalValueValues = new ArrayList<>();

            //6 arrays 1 per cada conjunt de valors
            for (Rate r : rateList) {

                categoryActingValues.add(r.getRateActing());
                categoryVisualsValues.add(r.getRateVisuals());
                categoryEditionValues.add(r.getRateEdition());
                categoryScriptValues.add(r.getRateScript());
                categorySoundValues.add(r.getRateSound());
                categoryGlobalValueValues.add(r.getRateGlobalValue());
            }

            medianValues[GLOBAL_VALUE] = calcCategoryMedian(categoryGlobalValueValues);
            medianValues[ACTING] = calcCategoryMedian(categoryActingValues);
            medianValues[VISUALS] = calcCategoryMedian(categoryVisualsValues);
            medianValues[EDITION] = calcCategoryMedian(categoryEditionValues);
            medianValues[SCRIPT] = calcCategoryMedian(categoryScriptValues);
            medianValues[SOUND] = calcCategoryMedian(categorySoundValues);

            updatedGlobalRate.setRateGlobalValue(medianValues[GLOBAL_VALUE]);
            updatedGlobalRate.setRateActing(medianValues[ACTING]);
            updatedGlobalRate.setRateVisuals(medianValues[VISUALS]);
            updatedGlobalRate.setRateEdition(medianValues[EDITION]);
            updatedGlobalRate.setRateScript(medianValues[SCRIPT]);
            updatedGlobalRate.setRateSound(medianValues[SOUND]);

        } else {

            updatedGlobalRate.setRateGlobalValue(rate.getRateGlobalValue());
            updatedGlobalRate.setRateActing(rate.getRateActing());
            updatedGlobalRate.setRateVisuals(rate.getRateVisuals());
            updatedGlobalRate.setRateEdition(rate.getRateEdition());
            updatedGlobalRate.setRateScript(rate.getRateScript());
            updatedGlobalRate.setRateSound(rate.getRateSound());
        }

        return updatedGlobalRate;
    }
}
