/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

import com.filmvalue.model.Film;
import com.filmvalue.model.Rate;
import java.util.List;

/**
 *
 * @author Oriol
 */

public interface FilmService {

    Boolean existsById(String filmId);
    Film getFilmById(String filmId);
    void saveFilm(Film film);
    void updateFilmRate(Rate rate);
    List<Film> getTopTen();
    List<Film> getTopCategory(String category);
    
}
