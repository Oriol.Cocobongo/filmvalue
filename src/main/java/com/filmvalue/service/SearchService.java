/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

import com.filmvalue.model.Film;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Oriol
 */
public interface SearchService {
    List<Film> getDadesFilmsSearch (String title);
    Map<String,String> getAllInfoMovie(String idFilm);
}
