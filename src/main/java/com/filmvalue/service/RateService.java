/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.service;

import com.filmvalue.model.Rate;
import java.util.List;

/**
 *
 * @author Oriol
 */
public interface RateService {
    //void saveRate(Rate rate);
    void saveRate(Rate rate, String idFilm);
    Rate calcNewFilmValueRate(Rate rate);
    List<Rate> getFilmRates(String idFilm);
    Rate getRateById(Long idRate);
    float[] getNewFilmValue(String idFilm);
}
