package com.filmvalue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmvalueApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilmvalueApplication.class, args);
	}

}
