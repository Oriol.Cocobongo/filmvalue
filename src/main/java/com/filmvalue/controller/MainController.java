/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.filmvalue.model.Film;
import com.filmvalue.model.Rate;
import com.filmvalue.model.User;

import com.filmvalue.service.FilmService;
import com.filmvalue.service.RateService;
import com.filmvalue.service.SearchService;
import com.filmvalue.service.UserService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @Autowired
    RateService rateService;

    @Autowired
    SearchService searchService;

    @Autowired
    FilmService filmService;

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String getIndexPage(Model model) {

        List<Film> topTen = filmService.getTopTen();

        if (topTen != null) {
            model.addAttribute("topTen", topTen);
        }

        return "index";
    }

    @GetMapping("/vote")
    public String showRateForm(@RequestParam("idFilm") String idFilm,
            @ModelAttribute("filmInfo") Map<String, String> filmInfo,
            Model model) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("idFilm", idFilm);
        model.addAttribute("filmInfo", filmInfo);
        //model.addAttribute("voted", rateService.userRatedThisFilm(idFilm));
        model.addAttribute("voted", userService.userRatedThisFilm(userProfile,idFilm));
        model.addAttribute("rateForm", new Rate());

        return "voteFilm";
    }

    @PostMapping("/vote")
    public ModelAndView submitRateForm(@RequestParam("idFilm") String idFilm,
            @ModelAttribute("rateForm") Rate rateForm) {

        //rateService.saveRate(rateForm);
        rateService.saveRate(rateForm, idFilm);
        filmService.updateFilmRate(rateForm);

        System.out.println(rateForm.toString());

        ModelAndView modelview = new ModelAndView("/filmRatedOk");
        //modelview.getModelMap().addAttribute("listRates", rateService.getFilmRates(rateForm.getIdFilm()));
        modelview.getModelMap().addAttribute("listRates", rateService.getFilmRates(idFilm));

        return modelview;
    }

    @GetMapping("/list")
    public ModelAndView getResponseSearch(@RequestParam("title") String title) throws JsonProcessingException {

        ModelAndView modelview = new ModelAndView("/list");
        modelview.getModelMap().addAttribute("listFilms", searchService.getDadesFilmsSearch(title));
        return modelview;
    }
    
    @GetMapping("/top")
    public ModelAndView categoryTopFilms(@RequestParam("category") String category) {

        ModelAndView modelview = new ModelAndView("/list");
        modelview.getModelMap().addAttribute("listFilms", filmService.getTopCategory(category));
        return modelview;
    }
    

    @GetMapping("/info")
    public String getAllInfoMovie(@RequestParam("id") String idFilm,
            Model model) {

        /*ModelAndView modelview = new ModelAndView("/info");
        modelview.getModelMap().addAttribute("idFilm", idFilm);
        modelview.getModelMap().addAttribute("filmInfo", searchService.getAllInfoMovie(idFilm));
         */
        model.addAttribute("idFilm", idFilm);
        model.addAttribute("filmInfo", searchService.getAllInfoMovie(idFilm));
        model.addAttribute("filmRate", filmService.getFilmById(idFilm));
        model.addAttribute("rateList", rateService.getFilmRates(idFilm));

        return "info";
    }

    @ModelAttribute("filmInfo")
    public Map<String, String> populateFilmInfoList(String idFilm) {

        if (idFilm == null) {
            return null;
        } else {
            System.out.println("Populate modelattribute");
            return searchService.getAllInfoMovie(idFilm);
        }

    }
    
    @GetMapping("/news")
    public String getAllNews(){
        
        return "news";
    }
    
    @GetMapping("/newsFull")
    public String getNewsFullInfo(){
        
        return "newsFull";
    }
}
