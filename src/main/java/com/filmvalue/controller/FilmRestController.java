/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.controller;

import com.filmvalue.model.Film;
import com.filmvalue.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Grup 4
 */
@RestController
@RequestMapping(path = "/api")
public class FilmRestController {

    @Autowired
    private FilmRepository filmRepository;

    @PostMapping("/addFilm")
    public Film addFilm(@RequestBody Film film) {
        return filmRepository.save(film);
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Film> getAllFilms() {
        return filmRepository.findAll();
    }

}
