/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.controller;

/**
 *
 * @author Oriol
 */
import com.filmvalue.model.User;
import com.filmvalue.service.SecurityService;
import com.filmvalue.service.UserService;
import com.filmvalue.validator.UserValidator;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
//@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "signup";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Your email or password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        return "login";
    }

    @GetMapping("/profile")
    public String profile(Model model,
            @ModelAttribute("userForm") User userForm) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("userProfile", userProfile);

        return "profile";
    }

    /*@PostMapping("/profile")
    public String editProfile(Model model,
            @ModelAttribute("userForm") User userForm,
            @RequestParam("file") MultipartFile file) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("userProfile", userProfile);
        
        if (!file.isEmpty()) {
            System.out.println("Image upload");
            Path imgDirectory = Paths.get("src//main//resources//static//profiles");
            String urlImgAbsolute =imgDirectory.toFile().getAbsolutePath();
            
            try {
                byte[] bytesImg = file.getBytes();
                Path completeUrl = Paths.get(urlImgAbsolute + "//" + file.getOriginalFilename());
                System.out.println(completeUrl);
                System.out.println(file.getOriginalFilename());
                Files.write(completeUrl, bytesImg);
                
                userProfile.setProfilePicture(file.getOriginalFilename());
                userService.updateUserProfile(userProfile);
                
            } catch (IOException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "profile";
    }*/
    
    @GetMapping("/profile/edit")
    public String editProfile(Model model,
            @ModelAttribute("userForm") User userForm) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("userProfile", userProfile);
        
        

        return "profileEdit";
    }
    
    @PostMapping("/profile/edit")
    public String editProfile(Model model,
            @ModelAttribute("userForm") User userForm,
            @RequestParam("file") MultipartFile file) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("userProfile", userProfile);
        
        if (!file.isEmpty()) {
            System.out.println("Image upload");
            Path imgDirectory = Paths.get("src//main//resources//static//profiles");
            //Path imgDirectory = Paths.get("src//main//resources//images");
            String urlImgAbsolute =imgDirectory.toFile().getAbsolutePath();
            
            try {
                byte[] bytesImg = file.getBytes();
                Path completeUrl = Paths.get(urlImgAbsolute + "//" + file.getOriginalFilename());
                System.out.println(completeUrl);
                System.out.println(file.getOriginalFilename());
                Files.write(completeUrl, bytesImg);
                
                userProfile.setProfilePicture(file.getOriginalFilename());
                userService.updateUserProfile(userProfile);
                
            } catch (IOException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "profileEdit";
    }

    @GetMapping("/history")
    public String getHistory(Model model) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User userProfile = userService.findByUsername(((UserDetails) principal).getUsername());

        model.addAttribute("userProfile", userProfile);

        return "history";
    }

    @GetMapping("/welcome")
    public String welcome(Model model) {
        return "wellcome";
    }
}
