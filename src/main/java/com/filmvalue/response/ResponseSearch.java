/*
 * FilmValue
 * Grup 4: Pau Sabater, Sergi Gorchs i Oriol Bagan
 * Projecte de desenvolupament d'aplicacions web - 2020
 */
package com.filmvalue.response;

import com.filmvalue.model.Film;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Sergi
 */
@Repository
public class ResponseSearch {

    /**
     * Obtenir les dades de la Api i adjuntar-hi les notes.
     *
     * @param title
     * @return
     * @throws JsonProcessingException
     */
    public List<Film> getDadesFilmsSearch(String title) throws JsonProcessingException {
        //Variables
        String urlBase = "http://www.omdbapi.com/?apikey=130764ff&s=" + title;
        List<Film> listFilms = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        // pretty print
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        //Obtenir dades com a text
        String dadesTxt = restTemplate.getForObject(urlBase, String.class);
        //Separar dades amb Map
        Map< String, ?> dadesMap = mapper.readValue(dadesTxt, Map.class);
        //Convertir a format string "JSON"
        String dadesJson = mapper.writeValueAsString(dadesMap.get("Search"));
        //Obtenir dades
        JsonNode dadesNode = mapper.readTree(dadesJson);
        for (JsonNode n : dadesNode) {
            System.out.println(n);
            //filtrem només pel·lícules
            if (n.get("Type").asText().equalsIgnoreCase("movie")) {
                Film film = new Film();
                film.setIdFilm(n.get("imdbID").asText());
                film.setTitle(n.get("Title").asText());
                film.setYear(n.get("Year").asText());
                film.setPoster(n.get("Poster").asText());
                film.setType(n.get("Type").asText());
                listFilms.add(film);
            }
        }
        return listFilms;
    }

    public Map getAllInfoMovie(String idFilm) throws JsonProcessingException {

        //Variables
        String url_base = "http://www.omdbapi.com/?apikey=130764ff&plot=full&i=" + idFilm;

        Map<String, String> filmInfo = new HashMap<String, String>(); //TO-DO ampliar obj Film amb aquests attr

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        // pretty print
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        //Obtenir dades com a text
        String dadesTxt = restTemplate.getForObject(url_base, String.class);
        //Separar dades amb Map
        Map< String, ?> dadesMap = mapper.readValue(dadesTxt, Map.class);

        for (Map.Entry<?, ?> entry : dadesMap.entrySet()) {
            if (entry.getKey().equals("Ratings")) {
//                //Valoracions d'altres plataformes que actualment no fem servir.
//                //Convertir a format string "JSON"
//                String dadesJson = mapper.writeValueAsString(dadesMap.get("Ratings"));
//                //Obtenir dades
//                JsonNode dadesNode = mapper.readTree(dadesJson);
//                for (JsonNode n : dadesNode) {
//                    System.out.println(n.get("Source").asText());
//                    System.out.println(n.get("Value").asText());
//                }
            } else {

                //Obtens les dades
                filmInfo.put((String) entry.getKey(), (String) entry.getValue());
                //System.out.println(entry.getKey() + " : " + entry.getValue());
                //System.out.println(entry.getValue());
            }
        }
        return filmInfo;
    }
}
