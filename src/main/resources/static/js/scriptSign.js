window.onload = inicialitza;

/**
 * Inicialitzar funcions
 */
function inicialitza() {
  //Variables
  const close_from = document.getElementsByClassName("close-form")[0]; //'X' per tancar.
  const bttnForm = document.getElementById("bttnForm"); //botó submit del formulari.

  //Dibuixar chart amb sigles 'FV'.
  generateReducedChart(7, 9, 6, 7, 8, 7, ".chartReduced");

  //AddEventListeners
  //Validar que els 'Passwords' siguin iguals.
  bttnForm.addEventListener("click", (e) => {
    validateForm(e);
  });
  //Tancar finestra 'Create Account' i tornar a la pàgina inicial.
  close_from.addEventListener("click", () => {
    window.location.replace("/");
  });
};

/*
* Generar gràfic amb sigles 'FV' automàticament.
*/
function generateReducedChart(
  photography,
  bso,
  acting,
  script,
  edition,
  globalValue,
  chartClass
) {
  let ctx = document.querySelector(chartClass).getContext("2d");
  let chart = new Chart(ctx, {
    // The type of chart we want to create
    type: "radar",

    // The data for our dataset
    data: {
      labels: ["", "", "", "", ""],
      datasets: [
        {
          label: "",
          borderColor: "rgb(190, 190, 190)",
          pointRadius: 0,
          borderWidth: 3,
          fill: false,
          data: [photography, script, edition, acting, bso],
        },
      ],
    },

    // Configuration options go here
    options: {
      legend: {
        display: false,
      },
      scale: {
        angleLines: {
          color: "rgb(190, 190, 190)", // lines radiating from the center
          lineWidth: 0.4,
        },
        gridLines: {
          lineWidth: 0.4,
          color: "rgb(150, 150, 150)",
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display: false,
        },
      },
    },
  });
}

/*
* Validar que els 'Passwords' siguin iguals.
*/
function validateForm(e) {
  //Variables
  const psswd = document.getElementById("pswd").value;
  const re_psswd = document.getElementById("re-pswd").value;
  const name = document.getElementById("name");
  const email = document.getElementById("email");

  //Obtenir valors de name i email per tornar a escriure'ls.
  let name_value = name.value;
  let email_value = email.value;

  if (psswd != re_psswd) {
    e.preventDefault();
    //Mostrar missatge d'error dels passwords i resetejar formulari.
    document.querySelector('.mssgForm p').style.visibility = "visible";
    document.querySelector('form[name^="form"]').reset();
    //Tornar a escriure valors de 'name' i 'email'.
    name.value = name_value;
    email.value = email_value;
  }
}