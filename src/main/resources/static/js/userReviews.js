
function isOverflown(element) {
  return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
}

//Fem visibles links readMore per als textos que superen el seu contenidor
var reviewTextContainerList = document.querySelectorAll(".reviewTextContainer");
var reviewContainerList = document.querySelectorAll(".reviewContainer");

function linkReadMore(){
for(let i=0;i<reviewTextContainerList.length;i++){
  if(isOverflown(reviewTextContainerList[i])){
    reviewTextContainerList[i].nextElementSibling.style.display = "block";
  }else{
    reviewTextContainerList[i].nextElementSibling.style.display = "none";
  }
 }
}
//Executem funció a l'inici i cada cop que canvia mesures pantalla:

window.addEventListener("resize", linkReadMore);
linkReadMore();
var readMoreList = document.querySelectorAll(".readMore");
//Afegim funcionalitat per desplegar contenidor text:
for(let i=0;i<reviewTextContainerList.length;i++){
     readMoreList[i].addEventListener("click", function(event){
       //Esborrem primer qualsevol altra caixa desplegada anteriorment:
       //unFoldBack('freeHeight',reviewContainerList,reviewTextContainerList);
       reviewTextContainerList[i].classList.toggle('freeHeight');
       reviewContainerList[i].classList.toggle('freeHeight');
       readMoreList[i].style.display = 'none';
     });
   }


//Funció per tornar a plegar
function unFoldBack(className,parentList,childList){
  for(let i=0;i<parentList.length;i++){
   if(parentList[i].classList.contains(className)){
    parentList[i].classList.remove(className);
    }
    for(let i=0;i<childList.length;i++){
     if(childList[i].classList.contains(className)){
      childList[i].classList.remove(className);
      readMoreList[i].style.display = 'block';
      }
  }
}
}
