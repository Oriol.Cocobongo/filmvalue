
// Càrrega CHART PRINCIPAL
var chartFilm = document.querySelector(".chartPlantillaFilm");
var valuesListFilm = document.querySelectorAll(".sectionValue .value");
var btnList = document.querySelectorAll(".-btn");
//
setTimeout(function(){
  // values  (visuals - script - editing - acting - sound)
  if(valuesListFilm[0].innerHTML != "-"){
   generateChartPlantilla(valuesListFilm[0].innerHTML,valuesListFilm[1].innerHTML,valuesListFilm[2].innerHTML,valuesListFilm[3].innerHTML,valuesListFilm[4].innerHTML,".chartPlantillaFilm");
 }else{
   //Opció si la pel·lícula no s'ha valorat, generem gràfic buit i canviem text botó rate:
   generateChartPlantilla(0,0,0,0,0,".chartPlantillaFilm");
   btnList[0].innerHTML = "Be the first to rate!";
   btnList[1].innerHTML = "Be the first to rate!";
 }
}, 500);

// Càrrega CHARTS CRITIQUES USUARIS:
var chartsUsersList = document.querySelectorAll(".chartUserWrap");
var chartsList = document.querySelectorAll(".chartUser");

for(let i=0;i<chartsUsersList.length;i++){
  let marksList = chartsUsersList[i].querySelectorAll(".value");
  let chart = chartsUsersList[i].querySelector(".chartUser");
  // values  (visuals - script - editing - acting - sound)
  generateChartUserNoBackgr(marksList[0].innerHTML,marksList[1].innerHTML,marksList[2].innerHTML,marksList[3].innerHTML,marksList[4].innerHTML,chartsList[i]);
}


//FUNCTION ON DARKMODE:
const darkSwitchIcon = document.querySelectorAll(".sunIconContainer");
for(let i=0; i<darkSwitchIcon.length;i++){
   darkSwitchIcon[i].addEventListener('click', darkTogglePlantilla, false);
}

function darkTogglePlantilla(){
  switchTheme();
  if(valuesListFilm[0].innerHTML !== "-"){
   generateChartPlantilla(valuesListFilm[0].innerHTML,valuesListFilm[1].innerHTML,valuesListFilm[2].innerHTML,valuesListFilm[3].innerHTML,valuesListFilm[4].innerHTML,".chartPlantillaFilm",true);
   for(let i=0;i<chartsUsersList.length;i++){
     let marksList = chartsUsersList[i].querySelectorAll(".value");
     let chart = chartsUsersList[i].querySelector(".chartUser");
     // values  (visuals - script - editing - acting - sound)
     generateChartUserNoBackgr(marksList[0].innerHTML,marksList[1].innerHTML,marksList[2].innerHTML,marksList[3].innerHTML,marksList[4].innerHTML,chartsList[i]);
   }
 }else{
   //Opció si la pel·lícula no s'ha valorat, generem gràfic buit i canviem text botó rate:
   generateChartPlantilla(0,0,0,0,0,".chartPlantillaFilm",true);
 }
}