//CHARTS
//Extraiem dades gràfics per generar-los
var chartContainersList = document.querySelectorAll(".chartUserWrap");
var notRatedList = document.querySelectorAll(".extraInfoRatedYet");

function generateCharts(){
for(let i=0;i<chartContainersList.length;i++){
  let marksList = chartContainersList[i].querySelectorAll(".value");
  if((marksList[0].innerHTML  || 0 !== marksList[0].innerHTML.length)){
    let chart = chartContainersList[i].querySelector(".chartUser");
  // values  (visuals - script - editing - acting - sound)
  generateChartUser(marksList[0].innerHTML,marksList[1].innerHTML,marksList[2].innerHTML,marksList[3].innerHTML,marksList[4].innerHTML,chart);
 }else{
  notRatedList[i].style.display = "flex";
  chartContainersList[i].style.display = "none";
 }
}
}
generateCharts();

//DARKMODE FUNCTION
const darkSwitchIcon = document.querySelectorAll(".sunIconContainer");
for(let i=0; i<darkSwitchIcon.length;i++){
   darkSwitchIcon[i].addEventListener('click', darkTogglePlantilla, false);
}

function darkTogglePlantilla(){
  switchTheme();
  generateCharts();
}
