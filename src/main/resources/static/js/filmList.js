//Comprovem quina categoria està activa i dibuixem creu:
var listCross = document.querySelectorAll(".crossPath");
const linksCategories = document.querySelectorAll("a.listBestCat");
var activeCategory = "";
for(let i=0; i<linksCategories.length;i++){
    if(linksCategories[i].previousElementSibling.classList.contains('listActive')){
        activeCategory = linksCategories[i].previousElementSibling.innerHTML;
            //fem que les creus es dibuixin:
         setTimeout(function(){
            listCross[i*2].classList.toggle("drawAnimation");
            listCross[(i*2)+1].classList.toggle("drawAnimation2");
        }, 600);
    }
}
var titleCategory = document.querySelector("#titleCategory");
var graphList = document.querySelectorAll(".boxFilmList .chartUserWrap, .boxListFilters .chartUserWrap");

if(activeCategory == ""){
  titleCategory.innerHTML = 'Select your category:';
}else{
  titleCategory.innerHTML = 'Best on:';
  //Resaltem resultat gràfic categoria seleccionada:
}


switch(activeCategory){
  case 'visuals':
      for(let i=0; i<graphList.length; i++){
        graphList[i].firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.classList.add('selectedCategoryStyle');
      }
      break;
  case 'script':
      for(let i=0; i<graphList.length; i++){
        graphList[i].firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.classList.add('selectedCategoryStyle');
      }
      break;
  case 'edition':
      for(let i=0; i<graphList.length; i++){
        graphList[i].firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.classList.add('selectedCategoryStyle');
      }
      break;
  case 'acting':
      for(let i=0; i<graphList.length; i++){
        graphList[i].lastElementChild.previousElementSibling.previousElementSibling.classList.add('selectedCategoryStyle');
      }
      break;
  case 'sound':
      for(let i=0; i<graphList.length; i++){
        graphList[i].lastElementChild.classList.add('selectedCategoryStyle');
      }
      break;
  case 'global value':
      for(let i=0; i<graphList.length; i++){
       graphList[i].firstElementChild.nextElementSibling.classList.add('selectedGlobal');
      }
      break;
      }