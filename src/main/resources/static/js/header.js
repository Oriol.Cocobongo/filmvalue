const pageBody = document.getElementById("body");
//Header
var lastKnownScrollY = 0;
var currentScrollY = 0;
var ticking = false;
var idOfHeader = 'header';
var eleHeader = null;
var headerHeight = 0;
var hiddenSearch = document.querySelector(".hiddenSearchBarContainer");
var iconHiddenSearch = document.querySelector(".iconSearch.mobile")
var crossIcon = document.querySelector(".crossIcon");
var burger = document.querySelector(".hamburger");
var hiddenMenu = document.querySelector(".hiddenMenu");

const classes = {
  pinned: 'header-pin',
  unpinned: 'header-unpin',
};
function onScroll() {
  currentScrollY = window.pageYOffset;
  requestTick();
}
function requestTick() {
  if (!ticking) {
    requestAnimationFrame(update);
  }
  ticking = true;
}
function update() {
  if (currentScrollY < lastKnownScrollY) {
    pin();
  } else if (currentScrollY > lastKnownScrollY) {
    unpin();
  }
  lastKnownScrollY = currentScrollY;
  ticking = false;
}
function pin() {
  if (eleHeader.classList.contains(classes.unpinned)) {
    eleHeader.classList.remove(classes.unpinned);
    eleHeader.classList.add(classes.pinned);
  }
}
function unpin() {
  if (eleHeader.classList.contains(classes.pinned) || !eleHeader.classList.contains(classes.unpinned)) {
    eleHeader.classList.remove(classes.pinned);
    eleHeader.classList.add(classes.unpinned);
  }
}

  eleHeader = document.querySelector(".header");
  document.addEventListener('scroll', onScroll, false);

//Event per mostrar barra cerca per mobile:
 iconHiddenSearch.addEventListener("click", function(event){
  hiddenSearch.classList.toggle('searchBarSeen');
     });
//Event per tancar barra de cerca per mobile:
crossIcon.addEventListener("click", function(event){
 hiddenSearch.classList.toggle('searchBarSeen');
    });
//Event per desplegar menú mobile:
burger.addEventListener("click", function(event){
 hiddenMenu.classList.toggle("menuShown");
    });


 //Menú amagat mobile:
 var menuHiddenBackground = document.querySelector(".hiddenMenu .wrap");
 var burger = document.querySelector(".hamburger");
 var menuSeen = false;
 var backgroundEffect = document.querySelector(".hiddenMenu .outer-scratch");
 var menuElements = document.querySelector(".hiddenMenu .wrap.restHidden");
  burger.addEventListener("click", function(event){
    if(!menuSeen){
      menuSeen = true;
      pageBody.style.position = 'fixed';
      menuHiddenBackground.style.opacity = '1';
      setTimeout(function(){
        backgroundEffect.style.display = 'block';
        menuElements.style.opacity = '1';
      }, 500);
    }else{
      menuHiddenBackground.style.opacity = '0.5';
      menuSeen = false;
      backgroundEffect.style.display = 'none';
      pageBody.style.position = 'static';
      menuElements.style.opacity = '0';
    };
 });
