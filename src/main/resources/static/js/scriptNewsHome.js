window.onload = inicialitza;

/**
 * Inicialitzar funcions
 */
function inicialitza() {
  carregarDades();
}

/*
 * Consulta AJAX a la url.
 */
function carregarDades() {
  let url =
    "https://content.guardianapis.com/search?q=film&section=film&format=json&show-fields=headline,thumbnail,trailText&order-by=newest&api-key=557faa2e-0a7d-406e-9861-3efff8f3eea9";
  let httpRequest;

  if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
  } else {
    console.error("Error: This browser does not support AJAX.");
  }

  httpRequest.onreadystatechange = function () {
    let jsonResponse;

    if (this.readyState == 4 && this.status == 200) {
      jsonResponse = JSON.parse(this.responseText);
      processarResposta(jsonResponse);
    }
  };
  httpRequest.open("GET", url, true);
  httpRequest.send(null);
}

//Resposta a mostrar pel navegador
function processarResposta(resp) {
    const sectionNews = document.querySelector(".sectionNews");
    const containerCards = sectionNews.querySelector(".containerCards");
    const card = containerCards.querySelectorAll(".swiper-slide");

      card.forEach((element, index) => {
        let result = resp.response.results;
        let field = result[index].fields;

        setDataNews(result[index].id, field.thumbnail, field.headline, field.trailText, element);
      });

}

// Omplir dades de les notícies
function setDataNews(urlNews, thumbnail, headline, trailText, card) {
  let link = "/newsFull#" + urlNews;
  
  card.querySelector("img").setAttribute("src", thumbnail);
  card.querySelector("img").setAttribute("alt", headline);
  card.querySelector("h1").innerHTML = headline;
  card.querySelector("p").innerHTML = trailText;
  card.querySelectorAll("a")[0].setAttribute("href", link);
  card.querySelectorAll("a")[1].setAttribute("href", link);
}


