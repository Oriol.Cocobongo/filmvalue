
var darkModeOn = false;
var changeAnim = document.querySelector('.fullPageAnim');
var sunIcon = document.querySelectorAll('.sunIcon');
var moonIcon = document.querySelectorAll('.moonIcon');

const currentThemeCheck = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;

if (currentThemeCheck) {
    if (currentThemeCheck === 'dark') {
      darkModeOn = true;
    }else{
     for(let i=0;i<moonIcon.length;i++){   
      moonIcon[i].style.display = "block";
      sunIcon[i].style.display = "none";
    }
   }
}

function switchTheme(e) {
    if(!darkModeOn){
        document.documentElement.setAttribute('data-theme', 'dark');
        changeAnim.style.display = 'block';
        darkModeOn = true;
      for(let i=0;i<moonIcon.length;i++){  
        moonIcon[i].style.display = "none";
        sunIcon[i].style.display = "block";
        }
        localStorage.setItem('theme', 'dark'); //add this
        setTimeout(() => {
          changeAnim.style.display = 'none';
        }, 500);
      }else{
        document.documentElement.setAttribute('data-theme', 'light');
        darkModeOn = false;
        changeAnim.style.display = 'block';
        localStorage.setItem('theme', 'light');
      for(let i=0;i<moonIcon.length;i++){ 
        moonIcon[i].style.display = "block";
        sunIcon[i].style.display = "none";
        }
        setTimeout(() => {
          changeAnim.style.display = 'none';
        }, 500);
      }
}
