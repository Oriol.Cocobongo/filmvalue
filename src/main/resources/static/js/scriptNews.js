window.onload = inicialitza;

//DARKMODE FUNCTION:
const sunIconContainer = document.querySelectorAll(".sunIconContainer");
for(let i=0; i<sunIconContainer.length;i++){
  sunIconContainer[i].addEventListener('click', switchTheme, false);
  }

/**
 * Inicialitzar funcions
 */
function inicialitza() {
  carregarDades(); 
}

/*
 * Consulta AJAX a la url.
 */
function carregarDades() {
  let url =
    "https://content.guardianapis.com/search?q=film&section=film&format=json&show-fields=headline,thumbnail,trailText,byline,firstPublicationDate&order-by=newest&page-size=21&api-key=557faa2e-0a7d-406e-9861-3efff8f3eea9";
  let httpRequest;

  if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
  } else {
    console.error("Error: This browser does not support AJAX.");
  }

  httpRequest.onreadystatechange = function () {
    let response;

    if (this.readyState == 4 && this.status == 200) {
      response = JSON.parse(this.responseText);
      processarResposta(response);  
      hideLoading();
    } 
  };
  httpRequest.open("GET", url, true);
  httpRequest.send(null);
}

//Resposta a mostrar pel navegador
function processarResposta(resp) {
  const result = resp.response.results;

  result.forEach((el) => {
    let f = el.fields;
    createContainerArticle(
      el.id,   
      el.webUrl,
      f.headline,
      f.thumbnail,
      f.trailText,
      f.byline,
      f.firstPublicationDate
    );
  });
}

// Crear contenidor notícies
function createContainerArticle(id, webUrl, headline, thumbnail, trailText, byline, firstPublicationDate) {
  //Estructura contenidor
  let container = document.getElementById("newsContainer");
  let row = document.createElement("article");
  let cap = document.createElement("header");
  let titol = document.createElement("h2");
  let dadesArticle = document.createElement("address");
  let linkFullNew = document.createElement("a");
  let imatge = document.createElement("img");
  let paragraf = document.createElement("p");
  let peu = document.createElement("footer");
  let linkOrigen = document.createElement("a");
  let d = new Date(firstPublicationDate);
  // let publicationDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
  let publicationDate = d.toDateString();

  //Omplir dades notícia
  row.setAttribute("id", id);
  titol.textContent = headline;
  imatge.setAttribute("src", thumbnail);
  imatge.setAttribute("alt", headline);
  paragraf.textContent = trailText;
  linkFullNew.setAttribute("href", "/newsFull#" + id);
  linkOrigen.textContent = "-- > Original News";
  linkOrigen.setAttribute("href", webUrl);
  dadesArticle.textContent = "Published by " +  byline + " - " + publicationDate;


  linkFullNew.appendChild(titol);
  cap.appendChild(linkFullNew);
  peu.appendChild(dadesArticle);
  peu.appendChild(linkOrigen);
  
  row.appendChild(cap);
  row.appendChild(imatge);
  row.appendChild(paragraf);
  row.appendChild(peu);

  container.appendChild(row);
  container.style.opacity = 1; 
  document.querySelector(".titleNews").style.opacity = 1; 
}

function hideLoading() {
    document.getElementById('buscant').style.display = ' none';
}