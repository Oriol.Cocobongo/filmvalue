window.onload = inicialitza;

/**
 * Inicialitzar funcions
 */
function inicialitza() {
    //Variables
    const bttnFormPhoto = document.getElementById("bttnFormPhoto"); //botó submit canviar foto.
    const bttnFormSettings = document.getElementById("bttnFormSettings"); //botó validar i enviar form.
    const bttnCancelSettings = document.getElementById("bttnCancelSettings"); //botó cancelar.
    const interruptor = document.getElementsByClassName("switchPswd")[0]; //toggle switch.
    const switchPaswd = document.querySelector(".switchPswd input"); //valor toggle switch.
    let activar = true; //Estat dels inputs psswd.

    //Inicialitzar inputs.
    document.getElementById("file").value = ""; //input buscar fitxer.
    estatInputsPasswords(activar); //inputs dels passwords.
    //desactivar toggle switch
    if (activar == true) {
        switchPaswd.checked = false;
    }

    //AddEventListeners
    //Validar format de la foto
    bttnFormPhoto.addEventListener("click", (e) => {
        validatePhotoType(e);
        validatePhotoSize(e);
    });
    refreshImage();
    
    // Activar input pswds
    interruptor.addEventListener("change", function () {
        activar = !activar;
        estatInputsPasswords(activar);
        //Validar que els 'Passwords' siguin iguals.
        if (activar == false) {
            bttnFormSettings.addEventListener("click", (e) => {
                //Validar form i mantenir estat.
                verifyPassword(e);
            });
        }
    });
    //Botó cancelar settings
    bttnCancelSettings.addEventListener("click", function () {
        if (activar == false) {
            activar = !activar;
            estatInputsPasswords(activar);
        }
        window.history.back();
    });
}

/**
 * Contol del formulari
 */
//Validar format de foto
function validatePhotoType(e) {
  const photoName = document.getElementById("file").value;
  const indexDot = photoName.lastIndexOf(".") + 1;
  const imageType = photoName.substr(indexDot, photoName.length).toLowerCase();

  if (!(imageType == "jpg" || imageType == "jpg" ||
      imageType == "jpeg" || imageType == "png")) {
    e.preventDefault();
    //Mostrar missatge d'error i resetejar formulari.
    document.getElementsByClassName("mssgErrorPhoto")[0].style.visibility =
      "visible";
    document.getElementById("formChange").reset();
  }
}

//Validar mida de la foto
function validatePhotoSize(e) {
    const midaPhoto = 2;
    const photoName = document.getElementById("file");
    let mssg = document.getElementsByClassName("mssgErrorPhoto")[0];

    if ((photoName.files[0].size/1024/1024) > midaPhoto) {
        e.preventDefault();
        //Mostrar missatge d'error i resetejar formulari.
        mssg.innerHTML = "Oops! The photo is too heavy. Max: " + midaPhoto + "MB";
        mssg.style.visibility = "visible";
        document.getElementById("formChange").reset();
    }
}

//refresca imatge
function refreshImage() {
    let photoName = document.querySelector(".grid-image img");
    let photo = photoName.getAttribute('src');
    
    photoName.src = photo; 
    photoName.onerror = function() {
        setTimeout(refreshImage, 500);    
    }
}

//Obtenir valors actuals dels inputs i retornar-los.
function reurnValuesFormSettings() {
    //Variables
    const name = document.getElementById("name");
    const email = document.getElementById("email");
    let name_value = name.value;
    let email_value = email.value;
    //Mostrar missatge d'error dels passwords i resetejar formulari.
    document.getElementsByClassName("mssgError")[0].style.visibility = "visible";
    document.querySelector('form[name^="form"]').reset();
    //Tornar a escriure valors de 'name', 'email'.
    name.value = name_value;
    email.value = email_value;
}

//Mantenir estat del switch toggle.
function mantenirEstatSwitchToggle() {
    const switchPaswd = document.querySelector(".switchPswd input");

    switchPaswd.checked = true;
}

//Verificar passwords diferents.
function verifyPassword(e) {
    const psswd = document.getElementById("pswd").value;
    const re_psswd = document.getElementById("re-pswd").value;

    if (psswd != re_psswd) {
        e.preventDefault();
        reurnValuesFormSettings();
        mantenirEstatSwitchToggle();
    }
}

//Canviar estat input passwords.
function estatInputsPasswords(estat) {
    document.getElementById("pswd").disabled = estat;
    document.getElementById("re-pswd").disabled = estat;
}