
/*
 * Control dels RangeSliders
 */
//Get rangeSlider [id, value]
function getRangeSliderValues(el) {
  let outrange = [];
  let outId;

  outId = el.attr("id");
  outrange[0] = outId.substring(outId.length - 1);
  outrange[1] = el.val();

  return outrange;
};

//Set value rangeSliders
function setRangeSlidersValue(notes) {
  $('input[type=range]').each(function(index) {
    $(this).val(notes[index]);
  });
}

/*
 * Control del text de les puntuacions.
 */
//Set valors al chart
function setTextValueRangeSlider(notes) {
  $(".photographyGraph.value").text(notes[0]);
  $(".scriptGraph.value").text(notes[1]);
  $(".editingGraph.value").text(notes[2]);
  $(".actingGraph.value").text(notes[3]);
  $(".bsoGraph.value").text(notes[4]);
};

//Set valor total al chart
function setTextTotalValue(notes) {
  let total = 0.0;

  for (let i = 0; i < notes.length; i++) {
    total += parseFloat(notes[i]);
  }
  total /= notes.length;

  $(".textTotalValue").text(total.toFixed(1));
};

//Estableix els valors inicials
function setInitialValues(notes) {
  setRangeSlidersValue(notes);
  setTextValueRangeSlider(notes);
  setTextTotalValue(notes);
  drawChart(notes, true);
  $('textarea').val('');
}

/*
* Cancela la votació de la peli.
*/
function cancelaVotacio() {
  const notes = [5, 5, 5, 5, 5];
  setInitialValues(notes);
  //Torna a la pàgina home.
  window.location.replace("/");
}

/**
 * Dibuixa el gràfic CHART JS
 */
function drawChart(notes, shadow) {
  //VARIABLES
  var ctx = document.querySelector(".chartVote").getContext("2d");
  let visuals = notes[0];
  let script = notes[1];
  let edition = notes[2];
  let acting = notes[3];
  let sound = notes[4];
  let printShadow = shadow;
  //variables acabats: 
  let background;
  let graphBackground;
  let strokeGraph;
  let strokePent;
  let angleLines;
  let shadowColor;

  let darkActivated = darkModeActivated();

  if(darkActivated){
    background = 'rgba(12, 12, 12, 1)';
    graphBackground = 'rgba(155, 155, 155, 0.05)';
    shadowColor = 'rgba(0, 0, 0, 0)';
    angleLines = 'rgb(50, 50, 50)';
    strokePent = 'rgb(5, 5, 5)';
  }else{
    background = 'rgba(237, 237, 237, 1)';
    graphBackground = 'rgba(155, 155, 155, 0.2)';
    shadowColor = 'rgba(0, 0, 0, 0.25)';
    angleLines = 'rgb(150, 150, 150)';
    strokePent = 'rgb(220, 220, 220)';
  }
  //Dibuixa lombra del chart una sola vegada
  if (printShadow) {
    //Gradient -shadow
    const gradient = ctx.createRadialGradient(100, 100, 0, 100, 100, 141.42);
    gradient.addColorStop(0, "white");
    gradient.addColorStop(1, "rgba(40, 40, 40, 0.3)");
    Chart.pluginService.register({
      beforeDraw: (chart) => {
        const { ctx, scale, config } = chart;
        const { xCenter, yCenter, drawingArea: radius } = scale;
        var a = (Math.PI * 2) / 5;
        ctx.save();
        ctx.translate(xCenter, yCenter);
        ctx.rotate((53 * Math.PI) / 180);
        ctx.moveTo(radius, 0);
        for (var i = 1; i < 5; i++) {
          ctx.lineTo(radius * Math.cos(a * i), radius * Math.sin(a * i));
        }
        ctx.closePath();
        ctx.fillStyle = config.options.chartArea.backgroundColor;
        ctx.shadowColor = "rgba(0, 0, 0, 0.2)";
        ctx.shadowBlur = 20;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.fill();
        ctx.restore();
      },
    });
  }

  var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: "radar",

    // The data for our dataset
    data: {
      labels: ["", "", "", "", ""],
      datasets: [
        {
          label: "",
          backgroundColor: graphBackground,
          borderColor: "rgb(99, 99, 99)",
          pointRadius: 0,
          borderWidth: 0.6,
          //fill:false,
          data: [visuals, script, edition, acting, sound],
        },
      ],
    },

    // Configuration options go here
    options: {
      chartArea: {
        backgroundColor: background
      },
      legend: {
        display: false
      },
      scale: {
        angleLines: {
          color: angleLines, // lines radiating from the center
          lineWidth: 0.4
        },
        gridLines: {
          lineWidth: 1,
          color: strokePent
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display: false
        }
      }
    }
  });
};



/**
 * Inicialitzar funcions
 */
var inicialitza = function () {
  //variables
  const bttnCancel = document.getElementById("bttnCancel");
  const notes = [5, 5, 5, 5, 5];

  //Escriu valors inicials Chart/RangeSliders
  setInitialValues(notes);

  //AddEventListener
  //Canvi de valors del chart
  $(".slider").on({
    input: function () {
      let notaRange = getRangeSliderValues($(this));
      notes[notaRange[0]] = notaRange[1];
      setTextValueRangeSlider(notes);
    },
    click: function () {
      drawChart(notes, false);
      setTextTotalValue(notes);
    },
  });
  //Torna a dibuixar el chart.
  $(window).resize(function() {
    drawChart(notes, false);
  });
  //Cancela la voatció
  bttnCancel.addEventListener("click", function () {
    cancelaVotacio();
  });
//FUNCTION ON DARKMODE:
const darkSwitchIcon = document.querySelectorAll(".sunIconContainer");
for(let i=0; i<darkSwitchIcon.length;i++){
   darkSwitchIcon[i].addEventListener('click', darkTogglePlantilla, false);
}
//funció a executar al clicar icona dark mode: 
function darkTogglePlantilla(){
  switchTheme();
  drawChart(notes, false);
}
};

//DOM carregat.
$(document).ready(inicialitza);