
//DARK MODE CHECK:
function darkModeActivated(){
var darkCheck = localStorage.getItem('theme') ? localStorage.getItem('theme') : null;
var darkActivated;

if (darkCheck) {
    if (darkCheck === 'dark') {
      return true;
    }else{
      return false;
    }
  }
}

//CHART JS
// *** MAIN CHART:
  // values  (visuals - script - editing - acting - sound)

var generatedChart;

function generateChartPlantilla(visuals,script,edition,acting,sound,className,destroy){
if(destroy == true){
  console.log('cleaaar');
  generatedChart.destroy();
}

  let background;
  let graphBackground;
  let strokeGraph;
  let strokePent;
  let angleLines;
  let shadow;
  let ctx = document.querySelector(className).getContext('2d');
  ctx.clearRect(0, 0, document.querySelector(className).width, document.querySelector(className).height);

  let darkActivated = darkModeActivated();

  if(darkActivated){
    background = 'rgba(12, 12, 12, 1)';
    graphBackground = 'rgba(155, 155, 155, 0.05)';
    shadow = 'rgba(0, 0, 0, 0)';
    angleLines = 'rgb(50, 50, 50)';
    strokePent = 'rgb(12, 12, 12)';
  }else{
    background = 'rgba(242, 242, 242, 1)';
    graphBackground = 'rgba(155, 155, 155, 0.2)';
    shadow = 'rgba(0, 0, 0, 0.25)';
    angleLines = 'rgb(150, 150, 150)';
    strokePent = 'rgb(242, 242, 242)';
  }
//Gradient:
const gradient = ctx.createRadialGradient(100,100,0,100,100,141.42);
gradient.addColorStop(0, 'white');
gradient.addColorStop(1, 'rgba(40, 40, 40, 0.3)');

Chart.pluginService.register({
    beforeDraw: chart => {
        const { ctx, scale, config } = chart
        const { xCenter, yCenter, drawingArea: radius } = scale
          var a = (Math.PI * 2)/5;
          ctx.save();
          ctx.translate(xCenter,yCenter);
          ctx.rotate(53 * Math.PI / 180);
          ctx.moveTo(radius,0);
          for (var i = 1; i < 5; i++) {
            ctx.lineTo(radius*Math.cos(a*i),radius*Math.sin(a*i));
          }
          ctx.closePath();
          ctx.fillStyle = config.options.chartArea.backgroundColor
          ctx.shadowColor = 'rgba(0, 0, 0, 0.15)';
          ctx.shadowBlur = 20;
          ctx.shadowOffsetX = 0;
          ctx.shadowOffsetY = 0;
          ctx.fill()
          ctx.restore();
        }
});



generatedChart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['', '', '', '', ''],
        datasets: [{
            label: '',
            backgroundColor: graphBackground,
            borderColor: 'rgb(99, 99, 99)',
            pointRadius:0,
            borderWidth:0.6,
            //fill:false,
            data: [visuals, script, edition, acting, sound]
        }]
    },

    // Configuration options go here
    options: {
      chartArea: {
    backgroundColor: background
},
      legend: {
          display: false
        },
      scale: {
        angleLines: {
          color: angleLines, // lines radiating from the center
          lineWidth: 0.3
        },
        gridLines: {
            lineWidth: 1,
            color: strokePent
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display:false
    }
}
    }
});
}

//CHART MINI:

function generateReducedChart(photography,bso,acting,script,edition,chartObj){
let ctx = chartObj.getContext('2d');
let chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['', '', '', '', ''],
        datasets: [{
            label: '',
            borderColor: 'rgb(190, 190, 190)',
            pointRadius:0,
            borderWidth:1,
            fill:false,
            data: [photography, script, edition, acting, bso]
        }]
    },

    // Configuration options go here
    options: {
      legend: {
          display: false
        },
      scale: {
        angleLines: {
          color: 'rgb(190, 190, 190)', // lines radiating from the center
          lineWidth: 0.4
        },
        gridLines: {
            lineWidth: 0.4,
            color: 'rgb(150, 150, 150)'
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display:false
    }
}
    }
});
}


function generateChartUser(visuals,script,edition,acting,sound,chart){
let ctx = chart.getContext('2d');

let background;
let graphBackground;
let strokeGraph;
let strokePent;
let borderGraph;
let angleLines;
let shadow;
let pentBackground;

let darkActivated = darkModeActivated();

if(darkActivated){
  graphBackground = 'rgba(0, 0, 0, 0.14)';
  shadow = 'rgba(55, 55, 55, 0)';
  angleLines = 'rgb(12, 12, 12)';
  strokePent = 'rgb(12, 12, 12)';
  borderGraph = 'rgb(145, 145, 145)';
  pentBackground = 'rgba(37, 37, 37, 0.1)';
}else{
  graphBackground = 'rgba(160, 160, 160, 0.3)';
  shadow = 'rgba(55, 55, 55, 0.5)';
  angleLines = 'rgb(140, 140, 140)';
  strokePent = 'rgb(110, 110, 110)';
  borderGraph = 'rgb(70, 70, 70)';
  pentBackground = 'rgba(242, 242, 242, 0.1)';
}


Chart.pluginService.register({
    beforeDraw: chart => {
        const { ctx, scale, config } = chart
        const { xCenter, yCenter, drawingArea: radius } = scale
          var a = (Math.PI * 2)/5;
          ctx.save();
          ctx.translate(xCenter,yCenter);
          ctx.rotate(53 * Math.PI / 180);
          ctx.moveTo(radius,0);
          for (var i = 1; i < 5; i++) {
            ctx.lineTo(radius*Math.cos(a*i),radius*Math.sin(a*i));
          }
          ctx.closePath();
          ctx.fillStyle = config.options.chartArea.backgroundColor;
          ctx.fill()
          ctx.restore();
        }
});

let chartUser = new Chart(ctx, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['', '', '', '', ''],
        datasets: [{
            label: '',
            backgroundColor: graphBackground,
            borderColor: borderGraph,
            pointRadius:0,
            borderWidth:0.5,
            //fill:false,
            data: [visuals, script, edition, acting, sound]
        }]
    },

    // Configuration options go here
    options: {
      chartArea: {
    backgroundColor: pentBackground
},
      legend: {
          display: false
        },
      scale: {
        angleLines: {
          color: angleLines, // lines radiating from the center
          lineWidth: 0.4
        },
        gridLines: {
            lineWidth: 0.4,
            color: strokePent
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display:false
    }
   }
 }
});
}

function generateChartUserNoBackgr(visuals,script,edition,acting,sound,chart){
let ctx = chart.getContext('2d');

let background;
let graphBackground;
let strokeGraph;
let strokePent;
let borderGraph;
let angleLines;
let shadow;

let darkActivated = darkModeActivated();

if(darkActivated){
  //background = 'rgba(12, 12, 12, 1)';
  graphBackground = 'rgba(12, 12, 12, 0.2)';
  shadow = 'rgba(55, 55, 55, 0)';
  angleLines = 'rgb(12, 12, 12)';
  strokePent = 'rgb(12, 12, 12)';
  borderGraph = 'rgb(85, 85, 85)';
}else{
  //background = 'rgba(237, 237, 237, 1)';
  graphBackground = 'rgba(155, 155, 155, 0.2)';
  shadow = 'rgba(55, 55, 55, 0.5)';
  angleLines = 'rgb(140, 140, 140)';
  strokePent = 'rgb(110, 110, 110)';
  borderGraph = 'rgb(90, 90, 90)';
}


let chartUser = new Chart(ctx, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['', '', '', '', ''],
        datasets: [{
            label: '',
            backgroundColor: graphBackground,
            borderColor: borderGraph,
            pointRadius:0,
            borderWidth:0.6,
            //fill:false,
            data: [visuals, script, edition, acting, sound]
        }]
    },

    // Configuration options go here
    options: {
      chartArea: {
    backgroundColor: 'rgba(230, 230, 230, 0)',
    responsive: true,
    maintainAspectRatio: false,
},
      legend: {
          display: false
        },
      scale: {
        angleLines: {
          color: angleLines, // lines radiating from the center
          lineWidth: 0.4
        },
        gridLines: {
            lineWidth: 0.4,
            color: strokePent
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          maxTicksLimit: 1,
          display:false
    }
}
    }
});
}
