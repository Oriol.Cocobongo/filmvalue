//DARKMODE FUNCTION:
var sunIconContainer = document.querySelector(".sunIconContainer");
sunIconContainer.addEventListener('click', switchTheme, false);

//https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
let resizeTimer;
var mobileView;
if(window.innerWidth>900){
  mobileView = false;
}else{
  mobileView = true;
}
// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.body.classList.add("resize-animation-stopper");
clearTimeout(resizeTimer);
resizeTimer = setTimeout(() => {
  document.body.classList.remove("resize-animation-stopper");
}, 100);
  document.documentElement.style.setProperty('--vh', `${vh}px`);
  if(window.innerWidth>900){
    mobileView = false;
  }else{
    mobileView = true;
  };
});
// OBSERVERS SECCIÓ EFECTE MENÚ GRANULAT:
var backgroundAnimated = document.querySelector('.menuBackground .outer-scratch');
const options0 = {
  threshold: 0.35,
}
const target0 = document.querySelector('.menuBackground');
function handleIntersection0(entries) {
  entries.map((entry) => {
    if (entry.isIntersecting) {
      backgroundAnimated.style.display = 'block';
    }else{
      backgroundAnimated.style.display = 'none';
    }
  });
}
const observer0 = new IntersectionObserver(handleIntersection0, options0);
observer0.observe(target0);


// OBSERVERS SECCIÓ TOP FILMS
var titles = document.querySelectorAll('.sectionTitleContainer .sectionTitle');
const options = {
  threshold: 0.3,
}
// First we select the element we want to target
const target = document.querySelector('.sectionTopFilms');
// Next we want to create a function that will be called when that element is intersected
function handleIntersection(entries) {
  // The callback will return an array of entries, even if you are only observing a single item
  entries.map((entry) => {
    if (entry.isIntersecting) {
       titles[0].style.opacity = 1;
        }
  });
}
// Next we instantiate the observer with the function we created above
const observer = new IntersectionObserver(handleIntersection, options);
observer.observe(target);


// OBERVER SECCIÓ FILMS
var cardsCovers = document.querySelectorAll(".sectionTopFilms .containerCards.films .cardCover");
var firstCardColumn = document.querySelector(".sectionTopFilms .cardFirstContainer .secondCol");
const options2 = {
  threshold: 0.5,
}
const target2 = document.querySelector('.sectionTopFilms');
function handleIntersection2(entries) {
  entries.map((entry) => {
    if (entry.isIntersecting) {
      for(let i=0;i<cardsCovers.length;i++){
         cardsCovers[i].style.transform = 'translateY(-100%)';
      }
      setTimeout(function(){ firstCardColumn.classList.toggle("active"); }, 300);
      observer2.unobserve(target2);
        }
  });
}
const observer2 = new IntersectionObserver(handleIntersection2, options2);
observer2.observe(target2);

// OBSERVERS SECCIÓ NEWS
const options3 = {
  threshold: 0.3,
}
const target3 = document.querySelector('.sectionNews');
function handleIntersection3(entries) {
  entries.map((entry) => {
    if (entry.isIntersecting) {
       titles[2].style.opacity = 1;
        }
  });
}
const observer3 = new IntersectionObserver(handleIntersection3, options3);
observer3.observe(target3);

// OBERVER CARROUSEL SECCIÓ FILMS
var cardsNewsCovers = document.querySelectorAll(".sectionNews .containerCards.films .cardCover");
var firstCardNewsColumn = document.querySelector(".sectionNews .cardFirstContainer .secondCol");
const options4 = {
  threshold: 0.5,
}
const target4 = document.querySelector('.sectionNews');
function handleIntersection4(entries) {
  entries.map((entry) => {
    if (entry.isIntersecting) {
      for(let i=0;i<cardsNewsCovers.length;i++){
         cardsNewsCovers[i].style.transform = 'translateY(-100%)';
      }
      setTimeout(function(){ firstCardNewsColumn.classList.toggle("active"); }, 300);
      observer4.unobserve(target4);
        }
  });
}
const observer4 = new IntersectionObserver(handleIntersection4, options4);
observer4.observe(target4);

/* CHARTS */
//data: [photography, script, edition, acting, bso]

//Extraiem dades html per crears els charts:
var graphElements = document.querySelectorAll(".sectionTopFilms .graphSection");
var chartList = document.querySelectorAll(".chartReduced");

for(let i=0;i<graphElements.length;i++){
  var marksList = graphElements[i].querySelectorAll(".graphValue");
  let chart = graphElements[i].querySelector(".chartReduced");
  // values  (visuals - script - editing - acting - sound)
  generateReducedChart(marksList[0].innerHTML,marksList[1].innerHTML,marksList[2].innerHTML,marksList[3].innerHTML,marksList[4].innerHTML,chart);
}

//Inimacions inicials
var menuAnimatedBackground = document.querySelector(".menuBackground .background");
var menuArea = document.querySelector(".menuBackground.overlay");
var categoriesCover = document.querySelectorAll(".categoriesCover");
var overlayMenuWords = document.querySelectorAll(".wordOverlay");
var wordsMenuClear = document.querySelectorAll(".wordClear");
var overlayWords = document.getElementById("wrapOverlayCat");
var titlesMenuFill = document.querySelectorAll(".titlesMenu.filled");

for(let i=0;i<categoriesCover.length;i++){
  categoriesCover[i].style.animation = "slideUp 1s ease-in 0.4s forwards";
}
setTimeout(function(){
  menuAnimatedBackground.style.opacity = 0.5;
}, 1500);

//ANIME ANIMATION
var animatedPolygon = document.querySelector(".animatedPolygon");

var points1 = "130.64 147.31 56.88 141.38 25.83 72.61 91.71 25.83 154.28 73.36 130.64 147.31";
var points2 = "126.64 141.16 47.74 154 21.72 71.69 91.71 34.89 157.59 72.61 126.64 141.16";
var points3 = "131.93 149.29 55.53 143.08 30.19 73.59 91.71 25.83 157.59 72.61 131.93 149.29";
var points4 = "123.6 136.48 60.6 135.27 20.87 71.5 91.71 31.39 150.58 74.19 123.6 136.48";
var points5 = "124.6 138.01 47.74 154 18.29 70.92 91.71 36.97 150.02 74.31 124.6 138.01";


function animateGraph(duration,target,value){
   const timeline = anime.timeline({
     loop: true,
     autoplay: true,
     duration:duration,
     direction: 'alternate',
     easing: 'linear'
   });
   timeline.add({
     targets:target,
     points: [
       {value: value},
       {value: points3},
       {value: points4},
       {value: points2},
       {value: points5},
     ]
   });
}
setTimeout(function(){
  animateGraph(10000,animatedPolygon,points1);
}, 2500);

//Events for overlay
for(let i=0;i<overlayMenuWords.length;i++){
  overlayMenuWords[i].addEventListener("mouseover", function(event){
      titlesMenuFill[i].style.opacity = '0.5';
      });
  overlayMenuWords[i].addEventListener("mouseleave", function(event){
      titlesMenuFill[i].style.opacity = '0';
  });
}

// ***** CAROUSEL ***** //

// link per crear i destruir swiper: https://medium.com/@networkaaron/swiper-how-to-destroy-swiper-on-min-width-breakpoints-a947491ddec8
//By Aaron K.

const breakpoint = window.matchMedia( '(min-width:900px)' );
//Guardem els links per crear o destruir-los segons la mida de pantalla:
var cardContainerList = document.querySelectorAll(".sectionTopFilms .swiper-slide");
var cardNewsContainerList = document.querySelectorAll(".sectionNews .swiper-slide");

// keep track of swiper instances to destroy later
let mySwiper;
let mySwiperNews;
//variables per canviar classes swiper

const breakpointChecker = function() {
   // if larger viewport and multi-row layout needed
   if ( breakpoint.matches === true ) {
      // clean up old instances and inline styles when available
      if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
      if ( mySwiperNews !== undefined ) mySwiperNews.destroy( true, true );
      clicknumber = 0;
      clickNewsNumber = 0;
      // or/and do nothing
      return;
   // else if a small viewport and single column layout needed
   } else if ( breakpoint.matches === false ) {
     clicknumber = 0;
     clickNewsNumber = 0;
      // fire small viewport version of swiper
      enableSwiperNews();
      return enableSwiper();
   }
};

const enableSwiper = function() {
   mySwiper = new Swiper('.swiper1', {
     slidesPerView: 2.3,
     slidesPerGroup: 2,
     momentumBounceRatio: 0.5,
     speed: 1000,
     pagination: {
       el: '.pagination1',
       clickable: true,
     },
     navigation: {
       nextEl: '.buttonNext1',
       prevEl: '.buttonPrev1',
     },
     breakpoints: {
       500: {
         slidesPerView: 3,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       600: {
         slidesPerView: 3,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       700: {
         slidesPerView: 4,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       800: {
         slidesPerView: 5,
         spaceBetween: 0,
       },
       1024: {
         slidesPerView: 5,
         spaceBetween: 0,
       },
     }
   });
}

const enableSwiperNews = function() {
   mySwiper = new Swiper('.swiper2', {
     slidesPerView: 2.3,
     slidesPerGroup: 2,
     momentumBounceRatio: 0.5,
     speed: 1000,
     pagination: {
       el: '.pagination2',
       clickable: true,
     },
     navigation: {
       nextEl: '.buttonNext2',
       prevEl: '.buttonPrev2',
     },
     breakpoints: {
       500: {
         slidesPerView: 3,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       600: {
         slidesPerView: 3,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       700: {
         slidesPerView: 4,
         spaceBetween: 0,
         slidesPerGroup: 2,
       },
       800: {
         slidesPerView: 5,
         spaceBetween: 0,
       },
       1024: {
         slidesPerView: 5,
         spaceBetween: 0,
       },
     }
   });
}

// keep an eye on viewport size changes
breakpoint.addListener(breakpointChecker);
// kickstart
breakpointChecker();

//SECCIÓ CARROUSEL MEDIAS +900PX:
var containerCards = document.querySelector(".containerCards.films");
var cardsList = document.querySelectorAll(".sectionTopFilms .card1");
var cardsBackground = document.querySelectorAll(".sectionTopFilms .clearBackground");
var cardsMainText = document.querySelectorAll(".sectionTopFilms .secondCol");
var activeCard = 0;
var icons = document.querySelectorAll(".iconCarousel");
var iconLeftCarouselFilms = icons[1];
var iconRightCarouselFilms = icons[3];
var iconLeftCarouselNews = icons[5];
var iconRightCarouselNews = icons[7];

var cardContainerList = document.querySelectorAll(".sectionTopFilms .swiper-slide");

//Afegim events per desplaçament al clicar les cards
for(let i=0;i<cardContainerList.length;i++){
  cardContainerList[i].addEventListener("click", function(event){
    //cas si no és vista per mòbil, funcionalitat per desplegar cards:
    if(!mobileView){
    //cas si no està activa
    if(!cardsList[i].firstElementChild.classList.contains('active')){
    //**********  cas si NO està desplegada:
    if(!cardsList[i].classList.contains("unfolded")){
      //cas click primera card i segona desplegada
      if((cardsList[i].classList.contains("firstCard"))&&(cardsList[i+1].classList.contains("unfolded"))){
        toggleCardsClassAfter(cardsList,i,"unfolded");
        if(!cardsMainText[i+1].classList.contains('active')){
          deleteClass("active",cardsBackground);
          deleteClass("active",cardsMainText);
        cardsMainText[i+1].classList.toggle("active");
        cardsBackground[i+1].classList.toggle("active");
        }
      }//si no es la primera i si la card anterior NO està desplegada, desplacem totes les cards anteriores menys la primera:
       else if(!cardsList[i-1].classList.contains("unfolded")){
          toggleCardsClassBefore(cardsList,i,"unfolded");
          deleteClass("active",cardsBackground);
          deleteClass("active",cardsMainText);
          activeCard = i;
          cardsMainText[i].classList.toggle("active");
          cardsBackground[i].classList.toggle("active");
        }

        //canviem nom classe per desplaçar si no es la primera
        if(!cardsList[i].classList.contains("firstCard")){
          cardsList[i].classList.toggle("unfolded");
          deleteClass("active",cardsBackground);
          deleteClass("active",cardsMainText);
          activeCard = i;
          cardsMainText[i].classList.toggle("active");
          cardsBackground[i].classList.toggle("active");

        }
        //Cas primera card:
        if(!cardsList[i].classList.contains("firstCard")){
           toggleCardsClassBefore(cardsList,i,"unfolded");
           deleteClass("active",cardsBackground);
           deleteClass("active",cardsMainText);
           activeCard = i;
           cardsMainText[i].classList.toggle("active");
           cardsBackground[i].classList.toggle("active");
         }
         //Cas si ja està desplagada:
       }else{
      if(i<(cardsList.length-1)) {
        if(cardsList[i+1].classList.contains("unfolded")){
            deleteClass("active",cardsBackground);
            deleteClass("active",cardsMainText);
          cardsMainText[i].classList.toggle("active");
          cardsBackground[i].classList.toggle("active");
          activeCard = i;
          toggleCardsClassAfter(cardsList,i,"unfolded");
         }
       }
    }
  }
  }
  });
}
//Cas clicar primera card:
var firstCard = document.querySelector('.sectionTopFilms .firstCard');
firstCard.addEventListener("click", function(event){
  toggleCardsClassAfter(cardsList,-1,"unfolded");
  deleteClass("active",cardsBackground);
  deleteClass("active",cardsMainText);
  cardsMainText[0].classList.toggle("active");
  cardsBackground[0].classList.toggle("active");
    });

    //Afegim funcionalitat al clicar sobre icona:
    var carouselFilmCounter = 0;
    var carouselTempFirst = 0;
    var pointsPagination = document.querySelectorAll(".sectionTopFilms .paginPointContainer .pointPaginEmpty:first-child");
    var clickNumber = 0;
    iconRightCarouselFilms.addEventListener("click", function(event){
       switch (clickNumber) {
         case 0:
         icons[0].style.opacity = '1';
         icons[0].style.cursor = 'pointer';
         icons[1].style.cursor = 'pointer';
         icons[1].style.display = 'block';
         containerCards.style.transform = "translate(-36vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[1].classList.toggle('pointShown');
         clickNumber++;
         break;
         case 1:
         containerCards.style.transform = "translate(-72vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[2].classList.toggle('pointShown');
         clickNumber++;
         break;
         case 2:
         icons[2].style.opacity = '0.2';
         icons[2].style.cursor = 'default';
         icons[3].style.cursor = 'default';
         icons[3].style.display = 'none';
         containerCards.style.transform = "translate(-108vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[3].classList.toggle('pointShown');
         clickNumber++;
         break;
       }
    });

    iconLeftCarouselFilms.addEventListener("click", function(event){
       switch (clickNumber) {
         case 1:
         containerCards.style.transform = "translate(0vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[0].classList.toggle('pointShown');
         icons[0].style.opacity = '0.2';
         icons[0].style.cursor = 'default';
         icons[1].style.cursor = 'default';
         icons[1].style.display = 'none';
         clickNumber--;
         break;
         case 2:
         containerCards.style.transform = "translate(-36vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[1].classList.toggle('pointShown');
         clickNumber--;
         break;
         case 3:
         icons[2].style.opacity = '1';
         icons[2].style.cursor = 'pointer';
         icons[3].style.cursor = 'pointer';
         icons[3].style.display = 'block';
         containerCards.style.transform = "translate(-72vw)";
         deleteClass('pointShown',pointsPagination);
         pointsPagination[2].classList.toggle('pointShown');
         clickNumber--;
         break;
       }
    });
    //Afegim funcionalitat al pagination:
    var containersPagination = document.querySelectorAll(".sectionTopFilms .paginPointContainer");
    for(let i=0;i<containersPagination.length;i++){
      containersPagination[i].addEventListener("click", function(event){
        switch (i) {
          case 0:
          icons[0].style.opacity = '0.2';
          icons[0].style.cursor = 'default';
          icons[1].style.cursor = 'default';
          icons[1].style.display = 'none';
          containerCards.style.transform = "translate(0vw)";
          deleteClass('pointShown',pointsPagination);
          pointsPagination[0].classList.toggle('pointShown');
          break;
          case 1:
          icons[0].style.opacity = '1';
          icons[0].style.cursor = 'pointer';
          icons[1].style.cursor = 'pointer';
          icons[1].style.display = 'block';
          icons[2].style.opacity = '1';
          icons[2].style.cursor = 'pointer';
          icons[3].style.cursor = 'pointer';
          icons[3].style.display = 'block';
          containerCards.style.transform = "translate(-36vw)";
          deleteClass('pointShown',pointsPagination);
          pointsPagination[1].classList.toggle('pointShown');
          break;
          case 2:
          icons[0].style.opacity = '1';
          icons[0].style.cursor = 'pointer';
          icons[1].style.cursor = 'pointer';
          icons[1].style.display = 'block';
          icons[2].style.opacity = '1';
          icons[2].style.cursor = 'pointer';
          icons[3].style.cursor = 'pointer';
          icons[3].style.display = 'block';
          containerCards.style.transform = "translate(-72vw)";
          deleteClass('pointShown',pointsPagination);
          pointsPagination[2].classList.toggle('pointShown');
          break;
          case 3:
          icons[2].style.opacity = '0.2';
          icons[2].style.cursor = 'default';
          icons[3].style.cursor = 'default';
          icons[3].style.display = 'none';
          containerCards.style.transform = "translate(-108vw)";
          deleteClass('pointShown',pointsPagination);
          pointsPagination[3].classList.toggle('pointShown');
          break;
        }
        clickNumber = i
      });
    }

    var cardsNewsList = document.querySelectorAll(".sectionNews .card1");
    var cardsNewsBackground = document.querySelectorAll(".sectionNews .clearBackground");
    var cardsNewsMainText = document.querySelectorAll(".sectionNews .secondCol");
    var activeCard = 0;

    for(let i=0;i<cardNewsContainerList.length;i++){
      cardNewsContainerList[i].addEventListener("click", function(event){
      });
    }

    //Afegim events per desplaçament al clicar les cards
    for(let i=0;i<cardNewsContainerList.length;i++){
      cardNewsContainerList[i].addEventListener("click", function(event){
        //cas si no és vista per mòbil, funcionalitat per desplegar cards:
        if(!mobileView){
        //cas si no està activa
        if(!cardsNewsList[i].firstElementChild.classList.contains('active')){
        //**********  cas si NO està desplegada:
        if(!cardsNewsList[i].classList.contains("unfolded")){
          //cas click primera card i segona desplegada
          if((cardsNewsList[i].classList.contains("firstCard"))&&(cardsNewsList[i+1].classList.contains("unfolded"))){
            toggleCardsClassAfter(cardsNewsList,i,"unfolded");
            if(!cardsNewsMainText[i+1].classList.contains('active')){
              deleteClass("active",cardsNewsBackground);
              deleteClass("active",cardsNewsMainText);
            cardsNewsMainText[i+1].classList.toggle("active");
            cardsNewsBackground[i+1].classList.toggle("active");
            }
          }//si no es la primera i si la card anterior NO està desplegada, desplacem totes les cards anteriores menys la primera:
           else if(!cardsNewsList[i-1].classList.contains("unfolded")){
              toggleCardsClassBefore(cardsNewsList,i,"unfolded");
              deleteClass("active",cardsNewsBackground);
              deleteClass("active",cardsNewsMainText);
              activeCard = i;
              cardsNewsMainText[i].classList.toggle("active");
              cardsNewsBackground[i].classList.toggle("active");
            }

            //canviem nom classe per desplaçar si no es la primera
            if(!cardsNewsList[i].classList.contains("firstCard")){
              cardsNewsList[i].classList.toggle("unfolded");
              deleteClass("active",cardsNewsBackground);
              deleteClass("active",cardsNewsMainText);
              activeCard = i;
              cardsNewsMainText[i].classList.toggle("active");
              cardsNewsBackground[i].classList.toggle("active");

            }
            //Cas primera card:
            if(!cardsNewsList[i].classList.contains("firstCard")){
               toggleCardsClassBefore(cardsNewsList,i,"unfolded");
               deleteClass("active",cardsNewsBackground);
               deleteClass("active",cardsNewsMainText);
               activeCard = i;
               cardsNewsMainText[i].classList.toggle("active");
               cardsNewsBackground[i].classList.toggle("active");
             }
             //Cas si ja està desplagada:
           }else{
          if(i<(cardsNewsList.length-1)) {
            if(cardsNewsList[i+1].classList.contains("unfolded")){
                deleteClass("active",cardsNewsBackground);
                deleteClass("active",cardsNewsMainText);
              cardsNewsMainText[i].classList.toggle("active");
              cardsNewsBackground[i].classList.toggle("active");
              activeCard = i;
              toggleCardsClassAfter(cardsNewsList,i,"unfolded");
             }
           }
        }
      }
    }
    });
    }
    //Cas clicar primera card:
    var firstNewsCard = document.querySelector('.sectionNews .firstCard');
    firstNewsCard.addEventListener("click", function(event){
      toggleCardsClassAfter(cardsNewsList,-1,"unfolded");
      deleteClass("active",cardsNewsBackground);
      deleteClass("active",cardsNewsMainText);
      cardsNewsMainText[0].classList.toggle("active");
      cardsNewsBackground[0].classList.toggle("active");
        });

        //Afegim funcionalitat al clicar sobre icona:
        var carouselNewsCounter = 0;
        var carouselTempFirst = 0;
        var containerCardsNews = document.querySelector(".sectionNews .containerCards");
        var pointsNewsPagination = document.querySelectorAll(".sectionNews .paginPointContainer .pointPaginEmpty:first-child");
        var pointsNewsPagination = document.querySelectorAll(".sectionNews .paginPointContainer .pointPaginEmpty:first-child");
        var clickNewsNumber = 0;
        iconRightCarouselNews.addEventListener("click", function(event){
           switch (clickNewsNumber) {
             case 0:
             icons[4].style.opacity = '1';
             icons[4].style.cursor = 'pointer';
             icons[5].style.cursor = 'pointer';
             icons[5].style.display = 'block';
             containerCardsNews.style.transform = "translate(-36vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[1].classList.toggle('pointShown');
             clickNewsNumber++;
             break;
             case 1:
             containerCardsNews.style.transform = "translate(-72vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[2].classList.toggle('pointShown');
             clickNewsNumber++;
             break;
             case 2:
             icons[6].style.opacity = '0.2';
             icons[6].style.cursor = 'default';
             icons[7].style.cursor = 'default';
             icons[7].style.display = 'none';
             containerCardsNews.style.transform = "translate(-108vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[3].classList.toggle('pointShown');
             clickNewsNumber++;
             break;
           }
        });

        iconLeftCarouselNews.addEventListener("click", function(event){
           switch (clickNewsNumber) {
             case 1:
             containerCardsNews.style.transform = "translate(0vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[0].classList.toggle('pointShown');
             icons[4].style.opacity = '0.2';
             icons[4].style.cursor = 'default';
             icons[5].style.cursor = 'default';
             icons[5].style.display = 'none';
             clickNewsNumber--;
             break;
             case 2:
             containerCardsNews.style.transform = "translate(-36vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[1].classList.toggle('pointShown');
             clickNewsNumber--;
             break;
             case 3:
             icons[6].style.opacity = '1';
             icons[6].style.cursor = 'pointer';
             icons[7].style.cursor = 'pointer';
             icons[7].style.display = 'block';
             containerCardsNews.style.transform = "translate(-72vw)";
             deleteClass('pointShown',pointsNewsPagination);
             pointsNewsPagination[2].classList.toggle('pointShown');
             clickNewsNumber--;
             break;
           }
        });
        //Afegim funcionalitat al pagination:
        var containersNewsPagination = document.querySelectorAll(".sectionNews .paginPointContainer");
        for(let i=0;i<containersNewsPagination.length;i++){
          containersNewsPagination[i].addEventListener("click", function(event){
            switch (i) {
              case 0:
              icons[4].style.opacity = '0.2';
              icons[4].style.cursor = 'default';
              icons[5].style.cursor = 'default';
              icons[5].style.display = 'none';
              containerCardsNews.style.transform = "translate(0vw)";
              deleteClass('pointShown',pointsNewsPagination);
              pointsNewsPagination[0].classList.toggle('pointShown');
              break;
              case 1:
              icons[4].style.opacity = '1';
              icons[4].style.cursor = 'pointer';
              icons[5].style.cursor = 'pointer';
              icons[5].style.display = 'block';
              icons[6].style.opacity = '1';
              icons[6].style.cursor = 'pointer';
              icons[7].style.cursor = 'pointer';
              icons[7].style.display = 'block';
              containerCardsNews.style.transform = "translate(-36vw)";
              deleteClass('pointShown',pointsNewsPagination);
              pointsNewsPagination[1].classList.toggle('pointShown');
              break;
              case 2:
              icons[4].style.opacity = '1';
              icons[4].style.cursor = 'pointer';
              icons[5].style.cursor = 'pointer';
              icons[5].style.display = 'block';
              icons[6].style.opacity = '1';
              icons[6].style.cursor = 'pointer';
              icons[7].style.cursor = 'pointer';
              icons[7].style.display = 'block';
              containerCardsNews.style.transform = "translate(-72vw)";
              deleteClass('pointShown',pointsNewsPagination);
              pointsNewsPagination[2].classList.toggle('pointShown');
              break;
              case 3:
              icons[6].style.opacity = '0.2';
              icons[6].style.cursor = 'default';
              icons[7].style.cursor = 'default';
              icons[7].style.display = 'none';
              containerCardsNews.style.transform = "translate(-108vw)";
              deleteClass('pointShown',pointsNewsPagination);
              pointsNewsPagination[3].classList.toggle('pointShown');
              break;
            }
            clickNewsNumber = i
          });
        }


function classBeforeChange(list, className, number){
  for(let i=number;i<list.length;i++){
   if(list[i].classList.contains(className)){
    list[i].classList.remove(className);
    }
  }
}

//Funció per afegir classe a totes les cards menys la card activa o la primera
function toggleAllExceptActive(list,className,numActive){
  for(let i=1;i<list.length;i++){
   if((!list[i].classList.contains(className))&&(i != numActive)){
    list[i].classList.toggle(className);
    }
  }
}

//Canviem la classe totes les cards anteriors per permetre el desplaçament
function toggleCardsClassBefore(list,number,className){
   for(let i=1;i<number;i++){
    if(!list[i].classList.contains(className)){
       list[i].classList.toggle(className);
     }
   }
}

//Canviem de classe totes les cards posteriors per permetre el desplaçament
function toggleCardsClassAfter(list,number,className,all){
   for(let i=(number+1);i<list.length;i++){
    if((list[i].classList.contains(className))||(all == true)){
     list[i].classList.toggle(className);
     }
   }
}

//Funció per esborrar classe d'una llista:
function deleteClass(className,list){
  for(let i=0;i<list.length;i++){
   if(list[i].classList.contains(className)){
    list[i].classList.remove(className);
    }
  }
}
