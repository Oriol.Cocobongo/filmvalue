window.onload = inicialitza;


/**
 * Inicialitzar funcions
 */
function inicialitza() {
    let idNews = getLocationHash();
    if (idNews) {
        carregarDades(idNews);
    } else {
        history.back();
    }
}

//DARKMODE FUNCTION:
const sunIconContainer = document.querySelectorAll(".sunIconContainer");
for(let i=0; i<sunIconContainer.length;i++){
  sunIconContainer[i].addEventListener('click', switchTheme, false);
  }
/*
 * Consulta AJAX a la url.
 */
function carregarDades(idNews) {
    let apiUrl = "https://content.guardianapis.com/";
    let url = apiUrl + idNews + "?format=json&show-fields=all&api-key=557faa2e-0a7d-406e-9861-3efff8f3eea9";
    let httpRequest;

    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        console.error("Error: This browser does not support AJAX.");
    }

    httpRequest.onreadystatechange = function () {
        let response;

        if (this.readyState == 4 && this.status == 200) {
            response = JSON.parse(this.responseText);
            processarResposta(response);
            hideLoading();
        }
    };
    httpRequest.onerror = function () {
        alert("Request failed");
    };
    httpRequest.open("GET", url, true);
    httpRequest.send(null);
}

//Resposta a mostrar pel navegador
function processarResposta(resp) {
    let content = resp.response.content;
    let field = content.fields;

    createContainerArticle(
            content.webUrl,
            field.headline,
            field.trailText,
            field.main,
            field.body,
            field.bylineHtml,
            field.firstPublicationDate
            );
}

// Crear contenidor notícies
function createContainerArticle(webUrl, headline, trailText, main, body, bylineHtml, firstPublicationDate) {
    //Estructura contenidor
    const container = document.getElementById("containerFullNews");
    const cap = container.getElementsByTagName("header")[0];
    const cos = container.getElementsByTagName("main")[0];
    const peu = container.getElementsByTagName("footer")[0];
    let title = cap.getElementsByTagName("h1")[0];
    let subtitle = cap.getElementsByTagName("p")[0];
    let fotoNews = cos.getElementsByClassName("fotoNews")[0];
    let txtNews = cos.getElementsByClassName("txtNews")[0];
    let addressAuthor = peu.getElementsByTagName("address")[0];
    let hiHaLinksAuthor;
    let linkAuthor;
    let newLinkAuthor;
    let dataPublicacio = peu.getElementsByTagName("time")[0];
    let d = new Date(firstPublicationDate);
    //let publicationDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    let publicationDate = d.toDateString();
    let linkOriginalNews = peu.getElementsByTagName("a")[0];

    //Set dades a la pàgina
    title.textContent = headline;
    subtitle.textContent = trailText;
    fotoNews.innerHTML = main;
    txtNews.innerHTML = body;
    //Get link autor article
    addressAuthor.innerHTML = bylineHtml;
    hiHaLinksAuthor = addressAuthor.getElementsByTagName("a");   
    if (hiHaLinksAuthor.length != 0) {
        linkAuthor = hiHaLinksAuthor[0].href;
        //Modificar link a the guardian
        newLinkAuthor = linkAuthor.replace('http://localhost:8080', 'https://www.theguardian.com');
        addressAuthor.getElementsByTagName("a")[0].setAttribute("href", newLinkAuthor);
    }
    
    //Set dades a la pàgina 
    linkOriginalNews.textContent = "Orignial News (The Guardian) - "
    dataPublicacio.innerHTML = publicationDate;
    linkOriginalNews.setAttribute("href", webUrl);
}

function getLocationHash() {
    const idNews = window.location.hash.substr(1);

    return idNews;
}

function hideLoading() {
    document.getElementById('buscant').style.display = 'none';
    document.getElementById('containerFullNews').style.display = 'block';
}

