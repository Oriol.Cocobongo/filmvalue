<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <link rel="stylesheet" href="/resources/css/styleSign.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>

        <title>FilmValue</title>
    </head>

    <body>

        <!-- Formulari Log in -->
        <div class="grid-form">
            <div class="item-form">
                <div class="close-form">X</div>
                <form id="formSignUp" name="formSignUp" action="${contextPath}/login" method="POST">
                    <div>
                        <h5>Log in</h5>
                        <h2>Film Value</h2>
                    </div>
                    <div class="simpleWrap">
                        <div class="graphSectionForm">
                            <canvas class="chartReduced"></canvas>
                            <p class="textTotalValueForm">FV</p>
                        </div>
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input name="username" type="email" id="email" required autofocus="true" minlength="6" maxlength="32">
                        <label for="pswd">Password</label>
                        <input type="password" id="pswd" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}" title="Please include at least 1 uppercase character, 1 lowercase character, and 1 number, and at least 8 to 32 characters">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>             
                    </div>
                    <div id="mssgLogin" class="mssgForm">
                        <span class="${message != null ? 'has-message' : ''}">${message}</span>
                        <span>${error}</span>
                    </div>
                    <div class="centerBttnForm">
                        <button id="bttnForm" type="submit" class="round buttonForm">Log in</button>
                    </div>
                    <div class="linkForm">
                        <p>New to FilmValue? <a class="linkActive" href="${contextPath}/registration"> Create account!</a></p>
                    </div>
                </form>
            </div>
        </div>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- javascripts  -->
        <script src="/resources/js/scriptSign.js"></script>
    </body>

</html>

