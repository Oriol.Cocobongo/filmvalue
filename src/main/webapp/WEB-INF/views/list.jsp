<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ca">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>


        <title>FilmValue</title>
    </head>
    <body id="body">

        <!-- Header -->
        <header class="header">
            <div class="wrap">
                <img class="logoIcon" alt="logo" src="/resources/svg/logoIcon.svg">
            </div>
            <div class="headerLogo">
                <a href="/">
                    <img class="logo" alt="FilmValue" src="/resources/svg/logo.svg">
                </a>
            </div>
            <div class="wrap"><a class="headerLink" href="/">home</a></div>
            <div class="wrap"><a class="headerLink" href="/news">news</a></div>
            <% if(request.getParameter("category") != null)  {%> 
               <div class="wrap"><a class="headerLink linkActive" href="/top?category=all">lists</a></div>
            <% } %>
            <% if(request.getParameter("category") == null)  {%> 
               <div class="wrap"><a class="headerLink" href="/top?category=all">lists</a></div>
            <% } %>
            <form for="search1" class="containerSearchBar" action="/list">
                <input id="search1" class="searchbar" name="title" type="text" placeholder="Search...">
                <button type="submit" style="display:none;">Search</button>
                <a class="btn-search">
                    <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                </a>
            </form>
            <div class="searchIconContainer">
                <img class="iconSearch mobile" alt="search" title="search" src="/resources/svg/iconSearch.svg">
            </div>
            <c:choose>
                <c:when test="${pageContext.request.userPrincipal.authenticated}">
                    <ul class="userDropdown">
                        <li>
                            <a class="firstDrop" href="#">
                                <img class="iconUser" alt="user" src="/resources/svg/iconUserFull.svg">
                            </a>
                            <ul class="dropdown">
                                <li><a href="${contextPath}/profile">Settings</a></li>
                                <li><a href="${contextPath}/history">History</a></li>
                                <hr>
                                <li>
                                    <form for="logOutInput" id="logoutForm" method="POST" action="${contextPath}/logout">
                                        <input id="logOutInput" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </form>
                                </li>
                                <li><a onclick="document.forms['logoutForm'].submit()" href="#">Log off</a></li>
                            </ul>
                        </li>
                    </ul>
                </c:when>
                <c:otherwise>
                    <div class="wrap signIn">
                        <a class="headerLink signInButton" href="${contextPath}/login">log in</a>
                    </div>
                </c:otherwise>
            </c:choose> 
            <div class="sunIconContainer headerDarkIcon">
               <div class="wrap">
                <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
            </div>
            </div>
            <!--<div class="wrap"><img class="iconUser" alt="user" title="user" src="/resources/svg/iconUser.svg"></div>--> 
            <div class="hiddenMenu">
                <div class="wrap">
                    <div class="outer-scratch">
                        <div class="inner-scratch">
                            <div class="background grain"></div>
                        </div>
                    </div>
                </div>

                <div class="wrap restHidden">
                    <div class="sunIconContainer headerMobileDarkIcon">
                        <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                        <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
                    </div>
                    <c:choose>
                        <c:when test="${pageContext.request.userPrincipal.authenticated}">
                            <ul class="categories catLogged">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                                <a class="titlesMenu"></a>
                                <a class="titlesMenu" href="${contextPath}/profile">User settings</a>
                                <a class="titlesMenu" href="/history">User history</a>
                                <a class="titlesMenu" onclick="document.forms['logoutForm'].submit()" href="#">Log off</a>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <div class="userIconContainer">
                                <a href="${contextPath}/login">
                                    <div class="wrap">
                                        <img class="signInIcon" alt="Sign In" src="/resources/svg/signIn.svg">
                                        <p class="headerLink">log in</p>
                                    </div>
                                </a>
                            </div>
                            <ul class="categories">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                    <img class="pentagonIcon" alt="search" src="/resources/svg/pentagonFV.svg">
                    <div class="graphAnimated"></div>
                </div>
            </div>
            <div class="burguerContainer">
                <input type="checkbox" id="checkbox3" class="checkbox3 visuallyHidden">
                <label for="checkbox3" aria-label="burguer">.
                    <div class="hamburger hamburger3">
                        <span class="bar bar1"></span>
                        <span class="bar bar2"></span>
                        <span class="bar bar3"></span>
                        <span class="bar bar4"></span>
                    </div>
                </label>
            </div>
            <div class="hiddenSearchBarContainer">
                <div class="wrap">
                    <form for="search2" class="containerSearchBar" action="/list">
                        <input id="search2" class="searchbar" name="title" type="text" placeholder="Search...">
                        <button type="submit" style="display:none;">Search</button>
                        <a class="btn-search">
                            <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                        </a>
                    </form>
                </div>
                <img class="crossIcon" alt="search" title="search" src="/resources/svg/cross.svg">
            </div>
        </header>
        <div class="fullPageAnim"></div>
        <div class="marginTop bigger"></div>

            <% if(request.getParameter("category") != null)  {%>                   
        <div class="boxListFilters">
            <div class="boxListAndFilm">
                <c:forEach begin="0" end="0" items="${listFilms}" var="film">
                    <div class="firstMovieContainer">
                        <div class="firstMovieCard">
                            <div class="clearBackground"></div>
                            <div class="cardPoster">
                                <c:choose>
                                    <c:when test="${film.poster.equals('N/A')}">
                                        <img class="cardPosterImg" src="/resources/svg/logoIcon.svg" alt="Photo not available">
                                    </c:when>
                                    <c:otherwise>
                                        <img class="cardPosterImg" src="${film.poster}" alt="Photo not available">
                                    </c:otherwise>
                                </c:choose> 
                                <div class="cardUnderlay"></div>
                                <div class="cardCover"></div>
                            </div>
                            <div class="firstCardTitleContainer">
                                <a href="/info?id=${film.idFilm}" class="firstCardTitle secondaryTitle">
                                    ${film.title}
                                </a>
                            </div>
                            <div class="cardNumberContainer">
                                <p class="cardNumber">1</p>
                            </div>
                            <div class="mainExplanationContainer">
                                <p class="textPrimary">${film.type} | ${film.year}</p>
                                <p class="textPrimary"><strong>Director: </strong>${film.director}</p>
                                <p class="textPrimary"><strong>Actors: </strong>${film.actors}</p>
                                <p class="textPrimary"><strong>Plot: </strong>${film.plot}</p>
                            </div>
                            <div class="buttonVoteContainer">
                                <a href="#" class="-btn buttonVote">
                                    Vote
                                </a>
                            </div>
                            <div class="borderDiv"></div>

                            <div class="chartUserWrap simpleWrap canvas">
                                <canvas class="chartUser"></canvas>
                                <p class="textTotalValue nonSelectedGlobal">${film.rateGlobalValue}</p>
                                <p class="textGraph photographyGraph catAbr">vis</p>
                                <p class="textGraph photographyGraph value nonSelectedCatStyle">${film.rateVisuals}</p>
                                <p class="textGraph scriptGraph catAbr">scr</p>
                                <p class="textGraph scriptGraph value nonSelectedCatStyle">${film.rateScript}</p>
                                <p class="textGraph editingGraph catAbr">ed</p>
                                <p class="textGraph editingGraph value nonSelectedCatStyle">${film.rateEdition}</p>
                                <p class="textGraph actingGraph catAbr">act</p>
                                <p class="textGraph actingGraph value nonSelectedCatStyle">${film.rateActing}</p>
                                <p class="textGraph bsoGraph catAbr">snd</p>
                                <p class="textGraph bsoGraph value nonSelectedCatStyle">${film.rateSound}</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>

                <div class="bestOnListContainer">
                    <div class="bestOnTitleContainer">
                        <h1 id="titleCategory" class="titleBestOn"></h1>
                    </div>
                    <div class="bestOnCatergory firstCat">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'acting'}">
                                <h1 class="listBestCat listFilled listActive">acting</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">acting</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=acting" class="listBestCat">acting</a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                    <div class="bestOnCatergory">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'visuals'}">
                                <h1 class="listBestCat listFilled listActive">visuals</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">visuals</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=visuals" class="listBestCat">visuals</h1></a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                    <div class="bestOnCatergory">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'edition'}">
                                <h1 class="listBestCat listFilled listActive">edition</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">edition</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=edition" class="listBestCat">edition</a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                    <div class="bestOnCatergory">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'script'}">
                                <h1 class="listBestCat listFilled listActive">script</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">script</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=script" class="listBestCat">script</a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                    <div class="bestOnCatergory">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'sound'}">
                                <h1 class="listBestCat listFilled listActive">sound</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">sound</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=sound" class="listBestCat">sound</a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                    <div class="bestOnCatergory">
                        <c:choose>
                            <c:when test="${pageContext.request.getParameter('category') == 'global'}">
                                <h1 class="listBestCat listFilled listActive">global value</h1>
                            </c:when><c:otherwise>
                                <h1 class="listBestCat listFilled">global value</h1>
                            </c:otherwise>
                        </c:choose>
                        <a href="top?category=global" class="listBestCat">global value</a>
                        <div class="cross">
                            <svg viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="crossPath" d="M1 13L11 1"/>
                            <path class="crossPath" d="M11 15L1 1"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="boxFilmList">
            <c:forEach begin="1" items="${listFilms}" var="film" varStatus="loop">
                <div class="filmListContainer">
                    <div class="clearBackground"></div>
                    <div class="cardNumberContainer">
                        <c:choose>
                            <c:when test="${loop.count!=9}">
                                <p class="cardNumber">${loop.count+1}</p>
                            </c:when>
                            <c:otherwise>
                                <p class="cardNumber numberMobile">10</p>
                                <p class="cardNumber lastNumberList">1<br>0</p>
                            </c:otherwise>
                        </c:choose> 
                    </div>
                    <div class="cardPoster">
                        <c:choose>
                            <c:when test="${film.poster.equals('N/A')}">
                                <img class="cardPosterImg" src="/resources/svg/logoIcon.svg" alt="Photo not available">
                            </c:when>
                            <c:otherwise>
                                <img class="cardPosterImg" src="${film.poster}" alt="Photo not available">
                            </c:otherwise>
                        </c:choose> 
                        <div class="cardUnderlay"></div>
                        <div class="cardCover"></div>
                    </div>
                    <div class="firstCardTitleContainer">
                        <a href="/info?id=${film.idFilm}" class="firstCardTitle secondaryTitle">
                            ${film.title}
                        </a>
                    </div>
                    <div class="filmListShortTextContainer textPrimary">
                        <p></p>
                        <p class="textPrimary">${film.type} | ${film.year}</p>
                        <p class="textPrimary"><strong>Director: </strong>${film.director}</p>
                        <p class="textPrimary"><strong>Actors: </strong>${film.actors}</p>
                    </div>
                    <div class="borderDiv"></div>
                    <div class="buttonVoteContainer">
                        <form action="vote">
                            <input type="hidden" name="idFilm" value="${film.idFilm}">
                            <button type="submit" class="-btn buttonVote">Vote</button>
                        </form>
                    </div>
                            <div class="chartUserWrap simpleWrap canvas">
                                <canvas class="chartUser"></canvas>
                                <p class="textTotalValue nonSelectedGlobal">${film.rateGlobalValue}</p>
                                <p class="textGraph photographyGraph catAbr">vis</p>
                                <p class="textGraph photographyGraph value nonSelectedCatStyle">${film.rateVisuals}</p>
                                <p class="textGraph scriptGraph catAbr">scr</p>
                                <p class="textGraph scriptGraph value nonSelectedCatStyle">${film.rateScript}</p>
                                <p class="textGraph editingGraph catAbr">ed</p>
                                <p class="textGraph editingGraph value nonSelectedCatStyle">${film.rateEdition}</p>
                                <p class="textGraph actingGraph catAbr">act</p>
                                <p class="textGraph actingGraph value nonSelectedCatStyle">${film.rateActing}</p>
                                <p class="textGraph bsoGraph catAbr">snd</p>
                                <p class="textGraph bsoGraph value nonSelectedCatStyle">${film.rateSound}</p>
                            </div>
                </div>
            </c:forEach>
        </div>
        <% } %>

        <% if(request.getParameter("category") == null)  {%>  
        <!-- Cas llistat per buscador per paraula -->
        <div class="boxFilmListSearch">
            <c:forEach items="${listFilms}" var="film">
                <div class="filmListContainer">
                    <div class="clearBackground"></div>
                    <div class="extraInfoResult">
                        <form action="vote">
                            <input type="hidden" name="idFilm" value="${film.idFilm}">
                            <button type="submit" class="-btn buttonVote btnList">Vote</button>
                        </form>
                    </div>
                    <!-- js list.js detecta si ha estat votada -->
                    <div class="extraInfoRatedYet">
                        <h1 class="textPrimary">
                            Not rated yet!
                        </h1>
                    </div>
                    <div class="borderDiv">
                    </div>
                    <div class="cardPoster">
                        <c:choose>
                            <c:when test="${film.poster.equals('N/A')}">
                                <img class="cardPosterImg" src="/resources/svg/logoIcon.svg" alt="Photo not available">
                            </c:when>
                            <c:otherwise>
                                <img class="cardPosterImg" src="${film.poster}">
                            </c:otherwise>
                        </c:choose> 
                        <div class="cardUnderlay"></div>
                        <div class="cardCover"></div>
                    </div>
                    <div class="searchInfoWrap">
                        <div class="listSearchTitle">
                            <a href="#" class="secondaryTitle listSearchtitle">
                                ${film.title}
                            </a>
                        </div>
                        <div class="listSearchInfo">
                            <div class="filmListShortTextContainer textPrimary">
                                <p class="textPrimary">${film.type} | ${film.year}</p>
                                <p class="textPrimary">${film.type}</p>
                                <p class="textPrimary"><strong>Year:</strong>${film.year}</p>
                                <p class="textPrimary"><strong>Votes: </strong>${film.totalRates}</p>
                            </div>
                        </div>
                    </div>
                    <div class="chartUserWrap simpleWrap canvas">
                        <canvas class="chartUser"></canvas>
                        <p class="textTotalValue">${film.rateGlobalValue}</p>
                        <p class="textGraph photographyGraph catAbr">vis</p>
                        <p class="textGraph photographyGraph value">${film.rateVisuals}</p>
                        <p class="textGraph scriptGraph catAbr">scr</p>
                        <p class="textGraph scriptGraph value">${film.rateScript}</p>
                        <p class="textGraph editingGraph catAbr">ed</p>
                        <p class="textGraph editingGraph value">${film.rateEdition}</p>
                        <p class="textGraph actingGraph catAbr">act</p>
                        <p class="textGraph actingGraph value">${film.rateActing}</p>
                        <p class="textGraph bsoGraph catAbr">snd</p>
                        <p class="textGraph bsoGraph value">${film.rateSound}</p>
                    </div>
                    <a href="/info?id=${film.idFilm}" class="containerLink"></a>
                </div>
            </c:forEach>
        </div>
        <% } %>


        <footer class="footer indexFooter">
            <p class="textPrimary">@FilmValue 2020 All rights reserved<br>
        </footer>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- javascripts  -->
        <script src="/resources/js/darkmode.js"></script>
        <script src="/resources/js/filmList.js"></script>
        <script src="/resources/js/script.js"></script>
        <script src="/resources/js/list.js"></script>
        <script src="/resources/js/header.js"></script>
    </body>
</html>
