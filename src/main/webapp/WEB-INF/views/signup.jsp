<%-- 
    Document   : signup
    Created on : 08-nov-2020, 23:28:14
    Author     : Sergi
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <link rel="stylesheet" href="/resources/css/styleSign.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>
        <title>FilmValue</title>
        
        
    </head>

    <body>

        <!-- Formulari Create Account -->
        <div class="grid-form">
            <div class="item-form">
                <div class="close-form">X</div>
                <form:form id="formSignUp" name="formSignUp" modelAttribute="userForm" method="POST">
                    <div>
                        <h5>Create Account</h5>
                        <h2>Film Value</h2>
                    </div>
                    <div class="simpleWrap">
                        <div class="graphSectionForm">
                            <canvas class="chartReduced"></canvas>
                            <p class="textTotalValueForm">FV</p>
                        </div>
                    </div>
                    <div>
                        <spring:bind path="name">
                            <form:label for="name" path="name">Name</form:label>
                            <form:input type="text" path="name" id="name" name="name" 
                                        autofocus="true" required="required" pattern="[a-zA-Z0-9]{2,32}" 
                                        title="Name should only contain letters and numbers, and at least 2 to 32 characters e.g. Ana or paolo34"/>
                        </spring:bind>
                        <spring:bind path="username">
                            <form:label for="email" path="username">Email</form:label>
                            <form:input type="email" path="username" id="email" name="username" 
                                        required="required" minlength="6" maxlength="32"/>
                        </spring:bind>
                        <spring:bind path="password">
                            <form:label for="pswd" path="password">Password</form:label>
                            <form:input type="password" path="password" id="pswd" name="password"
                                        required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}" 
                                        title="Please include at least 1 uppercase character, 1 lowercase character, and 1 number, and at least 8 to 32 characters"/>
                        </spring:bind>
                        <spring:bind path="passwordConfirm">  
                            <form:label for="re-pswd" path="passwordConfirm">Repeat Password</form:label>
                            <form:input type="password"  path="passwordConfirm" id="re-pswd" name="passwordConfirm"
                                        required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}" 
                                        title="Please include at least 1 uppercase character, 1 lowercase character, and 1 number, and at least 8 to 32 characters"/>
                        </spring:bind>    
                    </div>
                    <div id="mssgErrorSignUp" class="mssgForm">
                        <p>Oops! There was a problem with password confirmation.</p>
                    </div>
                    <div class="centerBttnForm">
                        <button id="bttnForm" type="submit" class="round buttonForm">Create account</button>
                    </div>
                </form:form>
            </div>
        </div>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- jQuery  -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <!-- javascripts  -->
        <script src="/resources/js/scriptSign.js"></script>
    </body>

</html>
