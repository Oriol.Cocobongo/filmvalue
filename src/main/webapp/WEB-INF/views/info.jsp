<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>
        <title>FilmValue</title>
    </head>
    <body id="body">
        <!-- Header -->
        <header class="header">
            <div class="wrap">
                <img class="logoIcon" alt="logo" src="/resources/svg/logoIcon.svg">
            </div>
            <div class="headerLogo">
                <a href="/">
                    <img class="logo" alt="FilmValue" src="/resources/svg/logo.svg">
                </a>
            </div>
            <div class="wrap"><a class="headerLink" href="/">home</a></div>
            <div class="wrap"><a class="headerLink" href="/news">news</a></div>
            <div class="wrap"><a class="headerLink" href="/top?category=all">lists</a></div>
            <form for="search1" class="containerSearchBar" action="/list">
                <input id="search1" class="searchbar" name="title" type="text" placeholder="Search...">
                <button type="submit" style="display:none;">Search</button>
                <a class="btn-search">
                    <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                </a>
            </form>
            <div class="searchIconContainer">
                <img class="iconSearch mobile" alt="search" title="search" src="/resources/svg/iconSearch.svg">
            </div>
            <c:choose>
                <c:when test="${pageContext.request.userPrincipal.authenticated}">
                    <ul class="userDropdown">
                        <li>
                            <a class="firstDrop" href="#">
                                <img class="iconUser" src="/resources/svg/iconUserFull.svg">
                            </a>
                            <ul class="dropdown">
                                <li><a href="${contextPath}/profile">Settings</a></li>
                                <li><a href="${contextPath}/history">History</a></li>
                                <hr>
                                <li>
                                    <form for="logOutInput" id="logoutForm" method="POST" action="${contextPath}/logout">
                                        <input id="logOutInput" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </form>
                                </li>
                                <li><a onclick="document.forms['logoutForm'].submit()" href="#">Log off</a></li>
                            </ul>
                        </li>
                    </ul>
                </c:when>
                <c:otherwise>
                    <div class="wrap signIn">
                        <a class="headerLink signInButton" href="${contextPath}/login">log in</a>
                    </div>
                </c:otherwise>
            </c:choose> 
            <div class="sunIconContainer headerDarkIcon">
               <div class="wrap">
                <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
            </div>
            </div>
            <!--<div class="wrap"><img class="iconUser" alt="user" title="user" src="/resources/svg/iconUser.svg"></div>--> 
            <div class="hiddenMenu">
                <div class="wrap">
                    <div class="outer-scratch">
                        <div class="inner-scratch">
                            <div class="background grain"></div>
                        </div>
                    </div>
                </div>

                <div class="wrap restHidden">
                    <div class="sunIconContainer headerMobileDarkIcon">
                        <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                        <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
                    </div>
                    <c:choose>
                        <c:when test="${pageContext.request.userPrincipal.authenticated}">
                            <ul class="categories catLogged">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                                <a class="titlesMenu"></a>
                                <a class="titlesMenu" href="${contextPath}/profile">User settings</a>
                                <a class="titlesMenu" href="/history">User history</a>
                                <a class="titlesMenu" onclick="document.forms['logoutForm'].submit()" href="#">Log off</a>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <div class="userIconContainer">
                                <a href="${contextPath}/login">
                                    <div class="wrap">
                                        <img class="signInIcon" alt="Sign In" src="/resources/svg/signIn.svg">
                                        <p class="headerLink">log in</p>
                                    </div>
                                </a>
                            </div>
                            <ul class="categories">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                    <img class="pentagonIcon" alt="search" src="/resources/svg/pentagonFV.svg">
                    <div class="graphAnimated"></div>
                </div>
            </div>
            <div class="burguerContainer">
                <input type="checkbox" id="checkbox3" class="checkbox3 visuallyHidden">
                <label for="checkbox3" aria-label="burguer">.
                    <div class="hamburger hamburger3">
                        <span class="bar bar1"></span>
                        <span class="bar bar2"></span>
                        <span class="bar bar3"></span>
                        <span class="bar bar4"></span>
                    </div>
                </label>
            </div>
            <div class="hiddenSearchBarContainer">
                <div class="wrap">
                    <form for="search2" class="containerSearchBar" action="/list">
                        <input id="search2" class="searchbar" name="title" type="text" placeholder="Search...">
                        <button type="submit" style="display:none;">Search</button>
                        <a class="btn-search">
                            <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                        </a>
                    </form>
                </div>
                <img class="crossIcon" alt="search" title="search" src="/resources/svg/cross.svg">
            </div>
        </header>
        <div class="fullPageAnim"></div>

        <!-- Contingut de la Info -->
        <div class="boxPlantilla">
            <div class="sectionTitleContainer">
                <div class="simpleWrap">
                    <h1 class="sectionTitle">${filmInfo.Title}</h1>
                </div>
                <p class="subFilmTitleText">
                    ${filmInfo.Rated}   |   ${filmInfo.Genre}   |   ${filmInfo.Runtime}   |   ${filmInfo.Year}    |   ${filmInfo.Country}
                </p>
                <p class="subFilmTitleText mobile"><strong>Votes</strong>: ${fn:length(rateList)} votes
                </p>
                <p class="subFilmTitleText mobile"><strong>Awards</strong>: ${filmInfo.Awards}
                </p>
            </div>
            <div class="cardPosterPlantilla">
                <c:choose>
                    <c:when test="${filmInfo.Poster.equals('N/A')}">
                        <img class="cardPosterImg" alt="Film poster" src="/resources/svg/logoIcon.svg" alt="Photo not available">
                    </c:when>
                    <c:otherwise>
                        <img class="cardPosterImg" src="${filmInfo.Poster}" alt="Film poster">
                    </c:otherwise>
                </c:choose>
                <!--<img class="cardPosterImg" src="${filmInfo.Poster}">-->
                <div class="cardCover"></div>
            </div>
            <div class="sectionFilmPlantilla">
                <div class="textFilmInfoComplete">
                    <div class="textPlot textPrimary">
                        <p><strong>Plot</strong>: ${filmInfo.Plot}</p>
                        <p><strong>Director</strong>: ${filmInfo.Director}</p>
                        <p><strong>Writer</strong>: ${filmInfo.Writer}</p>
                        <p><strong>Actors</strong>: ${filmInfo.Actors}</p>
                        <p><strong>Producer</strong>: ${filmInfo.Production}
                        </p>
                    </div>
                </div>

            </div>
            <div class="wrap filmChart">
                <div class="sectionValue">
                    <div class="simpleWrap canvas">
                        <canvas class="chartPlantillaFilm"></canvas>
                        <p class="textTotalValue">${filmRate.rateGlobalValue}</p>
                        <p class="textGraph photographyGraph">Visuals</p>
                        <p class="textGraph photographyGraph value">${filmRate.rateVisuals}</p>
                        <p class="textGraph scriptGraph">Script</p>
                        <p class="textGraph scriptGraph value">${filmRate.rateScript}</p>
                        <p class="textGraph editingGraph">Editing</p>
                        <p class="textGraph editingGraph value">${filmRate.rateEdition}</p>
                        <p class="textGraph actingGraph">Acting</p>
                        <p class="textGraph actingGraph value">${filmRate.rateActing}</p>
                        <p class="textGraph bsoGraph">Sound</p>
                        <p class="textGraph bsoGraph value">${filmRate.rateSound}</p>
                    </div>
                    <div class="extraInfo">
                        <div class="wrap textPrimary">
                            <p><strong>Votes</strong>: ${fn:length(rateList)} votes</p>
                            <p><strong>Awards</strong>: ${filmInfo.Awards}</p>
                            <form for="voteBtn1" action="vote">
                                <input id="voteBtn1" type="hidden" name="idFilm" value="${idFilm}">
                                <button type="submit" class="-btn">Rate movie</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="buttonValueMediaQueries">
                <form for="voteBtn2" action="vote">
                    <input id="voteBtn2" type="hidden" name="idFilm" value="${idFilm}">
                    <button type="submit" class="-btn">Rate movie</button>
                </form>
            </div>

        </div>
        <div class="boxUserReviews">
            <div class="sectionTitleContainer">
                <h1 class="sectionTitle">User reviews</h1>
                <h1 class="sectionTitle titleStroke">User reviews</h1>
            </div>

            <c:forEach items="${rateList}" var="rate">
                <div class="reviewContainer">
                    <div class="reviewUser">
                        <!--<a class="likeContainer" href="#">
                                <div class="wrap">
                                    <div class="likeIcon"></div>
                                    <div class="likeNumber"></div>
                                    <img class="likeIcon" src="/resources/img/likeIcon.png">
                                </div>
                            </a>-->
                        <img class="userImage" alt="*" src="/resources/profiles/${rate.user.profilePicture}">
                        <p class="reviewUserTitle">${rate.user.name}</p>
                    </div>
                    <div class="reviewTextContainer">
                        <p class="reviewText textPrimary">${rate.review}</p>
                    </div>
                    <p class="readMore">
                        + Read More
                    </p>
                    <div class="chartUserWrap simpleWrap canvas">
                        <canvas class="chartUser"></canvas>
                        <p class="textTotalValue">${rate.rateGlobalValue}</p>
                        <p class="textGraph photographyGraph catAbr">vis</p>
                        <p class="textGraph photographyGraph value">${rate.rateVisuals}</p>
                        <p class="textGraph scriptGraph catAbr">scr</p>
                        <p class="textGraph scriptGraph value">${rate.rateScript}</p>
                        <p class="textGraph editingGraph catAbr">ed</p>
                        <p class="textGraph editingGraph value">${rate.rateEdition}</p>
                        <p class="textGraph actingGraph catAbr">act</p>
                        <p class="textGraph actingGraph value">${rate.rateActing}</p>
                        <p class="textGraph bsoGraph catAbr">snd</p>
                        <p class="textGraph bsoGraph value">${rate.rateSound}</p>
                    </div>
                    <div class="userReviewBorder"></div>
                </div>
            </c:forEach>
        </div>

        <footer class="footer">
            <p class="textPrimary">@FilmValue 2020 All rights reserved<br>
        </footer>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- JavaScript -->
        <script src="/resources/js/script.js"></script>
        <script src="/resources/js/darkmode.js"></script>
        <script src="/resources/js/film.js"></script>
        <script src="/resources/js/userReviews.js"></script>
        <script src="/resources/js/header.js"></script>

    </body>
</html>
