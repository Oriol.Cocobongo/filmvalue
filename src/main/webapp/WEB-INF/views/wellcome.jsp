<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <link rel="stylesheet" href="/resources/css/styleSettings.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>


        <!-- Redirect to profile page -->
        <meta http-equiv="refresh" content="5; url='/profile'" />
        <title>FilmValue</title>
    </head>
    <body>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <form id="logoutForm" method="POST" action="${contextPath}/logout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <!-- Contingut Formulari settings -->
            <div class="grid-container">
                <!-- Header -->
                <div></div>

                <!-- Card amb foto i dades usuari -->
                <div class="grid-settings">
                    <!-- Dades de l'usuari -->
                    <div class="grid-title">
                        <h1>Welcome!</h1>
                    </div>
                    <div class="grid-imgForm">
                        <div class="grid-image">
                            <img src="/resources/svg/pentagonFV.svg" />
                        </div>
                        <div>
                            <p>In 5 seconds we will automatically redirect you to the profile page.</p>
                            <br>
                            <p>If you are not automatically redirected press the button.</p>
                            <br>
                            <div class="grid-bottom">
                                <a href="/profile">Profile Page</a>
                            </div>
                        </div>
                        <div class="profile">
                            <p>Email address</p>
                            <h3>${pageContext.request.userPrincipal.name}</h3>
                        </div>
                        <div class="grid-bottom">
                            <a onclick="document.forms['logoutForm'].submit()">Logout</a>
                        </div>
                    </div>
                </div>


                <!-- Footer -->
                <div></div>
            </div>
        </c:if>

        <footer class="footer indexFooter">
            <p class="textPrimary">@FilmValue 2020 All rights reserved<br>
        </footer>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- javascripts  -->
        <script src="/resources/js/darkmode.js"></script>
        <script src="/resources/js/scriptSettings.js"></script>
        <script src="/resources/js/header.js"></script>
    </body>
</html>