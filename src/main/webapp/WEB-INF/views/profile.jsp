<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/resources/img/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/resources/img/favicon.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap"
              rel="stylesheet">
        <!-- css -->
        <link rel="stylesheet" href="/resources/css/style.css">
        <link rel="stylesheet" href="/resources/css/styleSettings.css">
        <!-- script for localstorage -->
        <script src="/resources/js/localStorage.js"></script>


        <title>FilmValue</title>
    </head>

    <body id="body">

        <!-- Header -->
        <header id="header" class="header">
            <div class="wrap">
                <img class="logoIcon" src="/resources/svg/logoIcon.svg">
            </div>
            <div class="headerLogo">
                <a href="/">
                    <img class="logo" src="/resources/svg/logo.svg">
                </a>
            </div>
            <div class="wrap"><a class="headerLink" href="/">home</a></div>
            <div class="wrap"><a class="headerLink" href="/news">news</a></div>
            <div class="wrap"><a class="headerLink" href="/top?category=all">lists</a></div>
            <form id="formSearch" class="containerSearchBar" action="/list">
                <input id="searchBar" class="searchbar" name="title" type="text" placeholder="Search...">
                <button type="submit" style="display:none;">Search</button>
                <a id="btnSearch" class="btn-search">
                    <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                </a>
            </form>
            <div class="searchIconContainer">
                <img class="iconSearch mobile" alt="search" title="search" src="/resources/svg/iconSearch.svg">
            </div>
            <c:choose>
                <c:when test="${pageContext.request.userPrincipal.authenticated}">
                    <ul class="userDropdown">
                        <li>
                            <a class="firstDrop" href="#">
                                <img class="iconUser" src="/resources/svg/iconUserFull.svg">
                            </a>
                            <ul class="dropdown">
                                <li><a href="${contextPath}/profile">Settings</a></li>
                                <li><a href="${contextPath}/history">History</a></li>
                                <hr>
                                <li>
                                    <form id="logoutForm" method="POST" action="${contextPath}/logout">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </form>
                                </li>
                                <li><a onclick="document.forms['logoutForm'].submit()" href="#">Log off</a></li>
                            </ul>
                        </li>
                    </ul>
                </c:when>
                <c:otherwise>
                    <div class="wrap signIn">
                        <a class="headerLink signInButton" href="${contextPath}/login">log in</a>
                    </div>
                </c:otherwise>
            </c:choose> 
            <div class="sunIconContainer headerDarkIcon">
               <div class="wrap">
                <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
            </div>
            </div>
            <!--<div class="wrap"><img class="iconUser" alt="user" title="user" src="/resources/svg/iconUser.svg"></div>--> 
            <div class="hiddenMenu">
                <div class="wrap">
                    <div class="outer-scratch">
                        <div class="inner-scratch">
                            <div class="background grain"></div>
                        </div>
                    </div>
                </div>

                <div class="wrap restHidden">
                    <div class="sunIconContainer headerMobileDarkIcon">
                        <img class="sunIcon" src="/resources/svg/sun.svg" alt="bright mode">
                        <img class="moonIcon" src="/resources/svg/moon.svg" alt="bright mode" title="dark mode">
                    </div>
                    <c:choose>
                        <c:when test="${pageContext.request.userPrincipal.authenticated}">
                            <ul class="categories catLogged">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                                <a class="titlesMenu"></a>
                                <a class="titlesMenu" href="${contextPath}/profile">User settings</a>
                                <a class="titlesMenu" href="/history">User history</a>
                                <a class="titlesMenu" onclick="document.forms['logoutForm'].submit()" href="#">Log off</a>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <div class="userIconContainer">
                                <a href="${contextPath}/login">
                                    <div class="wrap">
                                        <img class="signInIcon" src="/resources/svg/signIn.svg">
                                        <p class="headerLink">log in</p>
                                    </div>
                                </a>
                            </div>
                            <ul class="categories">
                                <a class="titlesMenu" href="/">Home</a>
                                <a class="titlesMenu" href="/news">News</a>
                                <a class="titlesMenu" href="/top?category=all">Lists</a>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                    <img class="pentagonIcon" alt="search" src="/resources/svg/pentagonFV.svg">
                    <div class="graphAnimated"></div>
                </div>
            </div>
            <div class="burguerContainer">
                <input type="checkbox" id="checkbox3" class="checkbox3 visuallyHidden">
                <label for="checkbox3">
                    <div class="hamburger hamburger3">
                        <span class="bar bar1"></span>
                        <span class="bar bar2"></span>
                        <span class="bar bar3"></span>
                        <span class="bar bar4"></span>
                    </div>
                </label>
            </div>
            <div class="hiddenSearchBarContainer">
                <div class="wrap">
                    <form id="formSearch" class="containerSearchBar" action="/list">
                        <input id="searchBar" class="searchbar" name="title" type="text" placeholder="Search...">
                        <button type="submit" style="display:none;">Search</button>
                        <a id="btnSearch" class="btn-search">
                            <img class="iconSearch" alt="search" title="search" src="/resources/svg/iconSearch.svg">
                        </a>
                    </form>
                </div>
                <img class="crossIcon" alt="search" title="search" src="/resources/svg/cross.svg">
            </div>
        </header>
        <div class="fullPageAnim"></div>

        <!-- Contingut Formulari settings -->
        <div class="grid-container">
            <!-- Header -->
            <div></div>

            <!-- Card amb foto i dades usuari -->
            <div class="grid-settings">
                <!-- Dades de l'usuari -->
                <div class="grid-title">
                    <h1>User Profile</h1>
                </div>
                <div class="grid-imgForm">
                    <div class="grid-image">         
                        <img src="/resources/profiles/${userProfile.profilePicture}">
                    </div>
                    <div>
                        <p class="mssgErrorPhoto">Oops! There was a problem with the photo.</p>
                    </div>
                    <div class="profile">
                        <p>Display Name</p>
                        <h3>${userProfile.name}</h3>
                        <p>Email address</p>
                        <h3>${userProfile.username}</h3>
                    </div>
                    <div class="grid-bottom">
                        <a href="profile/edit">Edit profile</a>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            <div></div>
        </div>

        <footer class="footer indexFooter">
            <p class="textPrimary">@FilmValue 2020 All rights reserved<br>
        </footer>

        <!-- CHARTjs -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <!-- javascripts  -->
        <script src="/resources/js/darkmode.js"></script>
        <script src="/resources/js/darkmodeFunction.js"></script>
        <script src="/resources/js/scriptSettings.js"></script>
        <script src="/resources/js/header.js"></script>
    </body>
</html>